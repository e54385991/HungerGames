"Loot"
{
	"Tier0 weapon crate"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"physics/wood/wood_plank_break1.wav"
		
		"use"					"0"
		"touch"					"1"
		
		"weight"				"5"
		"round_start_weight"	"25"
		"max_spawned"			"100"
		
		"distance_min"			"0"
		"distance_max"			"512"
		
		"loottable"				"tier_0"
		
		"shop_price"			"100"
		"shop_price_dead"		"100"
		
		"msg"					"Loot_Tier0_Crate"
	}
	
	"Tier1 weapon crate"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"physics/wood/wood_plank_break1.wav"
		
		"use"					"0"
		"touch"					"1"
		
		"weight"				"10"
		"round_start_weight"	"5"
		"max_spawned"			"75"
		
		"distance_min"			"1500"
		"distance_max"			"3000"
		
		"loottable"				"tier_1"
		
		"shop_price"			"350"
		"shop_price_dead"		"350"
		
		"msg"					"Loot_Tier1_Crate"
	}
	
	"Tier2 weapon crate"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"physics/wood/wood_plank_break1.wav"
		
		"use"					"0"
		"touch"					"1"
		
		"weight"				"8"
		"round_start_weight"	"0"
		"max_spawned"			"50"
		
		"distance_min"			"2000"
		"distance_max"			"4000"
		
		"loottable"				"tier_2"
		
		"shop_price"			"750"
		"shop_price_dead"		"750"
		
		"msg"					"Loot_Tier2_Crate"
	}
	
	"Safe"
	{
		"model"					"models/custom_prop/safe/safe.mdl"
		
		"sound"					"ui/xp_levelup.wav"
		
		"use"					"0"
		"touch"					"0"
		"lockpicking"			"7.0"
		
		"weight"				"3"
		"round_start_weight"	"0"
		"max_spawned"			"3"
		
		"distance_min"			"3000"
		"distance_max"			"0"
		
		"loottable"				"safe"
		
		"points_min"			"15"
		"points_max"			"50"
		
		"shop_price"			"2000"
		"shop_price_dead"		"2000"
	
		"msg"					"Loot_Safe"
	}
	
	"Armor crate"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"hostage/huse/hostage_pickup.wav"
		
		"use"					"0"
		"touch"					"1"
		
		"weight"				"5"
		"round_start_weight"	"5"
		"max_spawned"			"100"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"armor"					"34"
		
		"shop_price"			"35"
		"shop_price_dead"		"35"
		"msg"					"Loot_Armor"
	}
	
	"Bread"
	{
		"model"					"models/props_misc/bread-4.mdl"
		
		"sound"					"hg/loot/eating.mp3"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"6"
		"round_start_weight"	"1"
		"max_spawned"			"50"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"hunger"				"100"
		
		"shop_price"			"45"
		"shop_price_dead"		"45"
		
		"msg"					"Loot_Bread"
	}
	
	"Banana"
	{
		"model"					"models/props/cs_italy/bananna_bunch.mdl"
		
		"sound"					"hg/loot/eating.mp3"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"9"
		"round_start_weight"	"1"
		"max_spawned"			"50"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"hunger"				"80"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_Banana"
	}
	
	"Orange"
	{
		"model"					"models/props/cs_italy/orange.mdl"
		
		"sound"					"hg/loot/eating.mp3"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"8"
		"round_start_weight"	"1"
		"max_spawned"			"50"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"hunger"				"40"
		"thirst"				"50"
		
		"shop_price"			""
		"shop_price_dead"		""
		"msg"					"Loot_Orange"
	}
	
	"Melon"
	{
		"model"					"models/props_junk/watermelon01.mdl"
		
		"sound"					"hg/loot/eating.mp3"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"8"
		"round_start_weight"	"3"
		"max_spawned"			"50"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"hunger"				"45"
		"thirst"				"80"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_Melon"
	}
	
	"Water"
	{
		"model"					"models/props/cs_office/water_bottle.mdl"
		
		"sound"					"hg/loot/drinking.mp3"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"10"
		"round_start_weight"	"1"
		"max_spawned"			"50"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"thirst"				"100"
		
		"shop_price"			"25"
		"shop_price_dead"		"25"
		
		"msg"					"Loot_Water"
	}
	
	"Energy Drink"
	{
		"model"					"models/props/cs_office/trash_can_p7.mdl"
		
		"sound"					"hg/loot/soda.mp3"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"5"
		"round_start_weight"	"3"
		"max_spawned"			"15"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"thirst"				"75"
		"stamina"				"100"
		
		"points_min"			"5"
		"points_max"			"15"
		
		"shop_price"			"75"
		"shop_price_dead"		"75"
		
		"msg"					"Loot_Energydrink"
	}
	
	"Healthkit"
	{
		"model"					"models/items/healthkit.mdl"
		
		"sound"					"items/medshot4.wav"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"5"
		"round_start_weight"	"1"
		"max_spawned"			"15"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"health"				"80"
		
		"shop_price"			"300"
		"shop_price_dead"		"250"
		
		"msg"					"Loot_Healthkit"
	}
	
	"Bandages"
	{
		"model"					"models/items/bandages/item_bandages.mdl"
		
		"sound"					"items/medshot4.wav"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"5"
		"round_start_weight"	"5"
		"max_spawned"			"20"
		
		"distance_min"			"512"
		"distance_max"			"0"
		
		"health"				"40"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_Bandages"
	}
	
	"Binoculars"
	{
		"model"					"models/custom_prop/binoculars/binoculars.mdl"
		
		"sound"					"items/ammopickup.wav"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"1"
		"round_start_weight"	"5"
		"max_spawned"			"10"
		
		"distance_min"			"2500"
		"distance_max"			"4000"
		
		"binoculars"			"1"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_Binoculars"
	}
	
	"Compass"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"ui/item_sticker_apply_confirm.wav"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"5"
		"round_start_weight"	"8"
		"max_spawned"			"10"
		
		"distance_min"			"0"
		"distance_max"			"2500"
		
		"compass"				"1"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_Compass"
	}
	
	"Tracer"
	{
		"model"					"models/custom_prop/laser/laser.mdl"
		
		"sound"					"ui/armsrace_level_up.wav"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"3"
		"round_start_weight"	"1"
		"max_spawned"			"10"
		
		"distance_min"			"3000"
		"distance_max"			"0"
		
		"tracer"				"1"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_Tracer"
	}
	
	"Jammer"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"hg/loot/jammer.mp3"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"2"
		"round_start_weight"	"1"
		"max_spawned"			"3"
		
		"distance_min"			"3000"
		"distance_max"			"0"
		
		"jammer"				"1"
		
		"points_min"			"5"
		"points_max"			"15"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_Jammer"
	}
	
	"GPS"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"ui/armsrace_become_leader_team.wav"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"2"
		"round_start_weight"	"1"
		"max_spawned"			"3"
		
		"distance_min"			"3000"
		"distance_max"			"0"
		
		"gps"					"1"
		
		"points_min"			"5"
		"points_max"			"15"
		
		"shop_price"			""
		"shop_price_dead"		""
		
		"msg"					"Loot_GPS"
	}
	
	"Proxy Mine"
	{
		"model"					"models/iSonY/mina/mina.mdl"
		
		"sound"					"weapons/c4/c4_explode1.wav"
		
		"use"					"0"
		"touch"					"1"
		"defuse"				"3.5"
		
		"weight"				"0"
		"round_start_weight"	"25"
		"max_spawned"			"50"
		
		"distance_min"			"512"
		"distance_max"			"0"
		
		"proxymine"				"1"
		
		"shop_price"			"100"
		"shop_price_dead"		"0"
		
		"msg"					"Loot_ProxyMine"
	}
	
	"Parachute"
	{
		"model"					"models/zipcore/woodcrate_32.mdl"
		
		"sound"					"ui/armsrace_become_leader_team.wav"
		
		"use"					"1"
		"touch"					"1"
		
		"weight"				"3"
		"round_start_weight"	"7"
		"max_spawned"			"15"
		
		"distance_min"			"1500"
		"distance_max"			"0"
		
		"parachute"				"1"
		
		"shop_price"			""
		"shop_price_dead"		""
	
		"msg"					"Loot_Parachute"
	}
}