Handle g_hGiveKnifeTimer[MAXPLAYERS+1] = {null, ...};

void GiveKnifeDelayed(int client)
{
	ResetGiveKnifeTimer(client);
	
	if (g_fGiveKnifeDelay >= 0.0)
		g_hGiveKnifeTimer[client] = CreateTimer(g_fGiveKnifeDelay, Timer_GiveKnive, client);
}

void ResetGiveKnifeTimer(int client)
{
	if(g_hGiveKnifeTimer[client] != null)
	{
		CloseHandle(g_hGiveKnifeTimer[client]);
		g_hGiveKnifeTimer[client] = null;
	}
}

public Action Timer_GiveKnive(Handle timer, any client)
{
	g_hGiveKnifeTimer[client] = null;
	
	if(client && IsClientInGame(client) && IsPlayerAlive(client))
	{
		GiveTributeKnife(client);
	}
	
	return Plugin_Handled;
}

void GiveTributeKnife(int client)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsTribute[client])
		return;
	
	bool bHasTaser;
	int iWeapon = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
	
	while (iWeapon != -1)
	{
		// check for taser
		if (GetEntProp(iWeapon, Prop_Send, "m_iItemDefinitionIndex") == 31)
			bHasTaser = true;
		
		RemovePlayerItem(client, iWeapon);
		AcceptEntityInput(iWeapon, "Kill");
		iWeapon = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
	}
	
	EquipPlayerWeapon(client, GivePlayerItem(client, "weapon_knife"));
	
	// Give back taser
	if (bHasTaser)
		GivePlayerItem(client, "weapon_taser");
}