void StartEarthquake()
{
	if(g_bSndEarthquake)
		EmitSoundToAllAny(sndEarthquake);
	
	float fCenter[3];
	float fBuffer[3];
	int iCount;
	
	int	entity;
	while ((entity = FindEntityByClassname(entity, "info_player_counterterrorist" )) != -1)
	{
		Entity_GetAbsOrigin(entity, fBuffer);
		AddVectors(fCenter, fBuffer, fCenter);
		
		iCount++;
	}
	
	while ((entity = FindEntityByClassname(entity, "info_player_terrorist" )) != -1)
	{
		Entity_GetAbsOrigin(entity, fBuffer);
		AddVectors(fCenter, fBuffer, fCenter);
		
		iCount++;
	}
	
	fCenter[0] /= iCount;
	fCenter[1] /= iCount;
	fCenter[2] /= iCount;
	
	Env_Shake(fCenter, 120.0, 4000.0, 15.0, 250.0);
	
	LoopTributes(client)
		MakeShakeScreen(client, g_fEventEarthquakeIntensity, g_fEventEarthquakeFrequency, g_fEventEarthquakeDuration);
}