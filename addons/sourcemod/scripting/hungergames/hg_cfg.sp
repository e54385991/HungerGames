ConVar cvPrefix;
char g_sPrefix[256];

ConVar g_cvEndgameMaxPlayers;
int g_iEndgameMaxPlayers;

ConVar g_cvEndgameMinDelay;
float g_fEndgameMinDelay;

ConVar g_cvCheckInterval;
float g_fCheckInterval;

ConVar g_cvRoundEndDelay;
float g_fRoundEndDelay;

ConVar g_cvDeathDelay;
float g_fDeathDelay;

ConVar cvDeathTeamChange;
bool g_bDeathTeamChange;

/* Give Items Delayed */

ConVar cvGiveKnifeDelay;
float g_fGiveKnifeDelay;

ConVar cvGiveBinocularsDelay;
float g_fGiveBinocularsDelay;

ConVar cvGiveCompassDelay;
float g_fGiveCompassDelay;

ConVar cvGiveParachuteDelay;
float g_fGiveParachuteDelay;

/* HUD */

ConVar cvHudTickInterval;
float g_fHudTickInterval;

ConVar cvHudMaxTicks;
int g_iHudMaxTicks;

ConVar cvHudEquipmentTicks;
int g_iHudEquipmentTicks;

ConVar cvHudCompassTicks;
int g_iHudCompassTicks;

/* Endgame */

ConVar cvBeaconInterval;
float g_fBeaconInterval;

ConVar cvKnifeFightDelay;
float g_fKnifeFightDelay;

ConVar cvEndgameWipeLoot;
bool g_bEndgameWipeLoot;

/* Weapon Cleaner */

ConVar cvWeaponCleanerInterval;
float g_fWeaponCleanerInterval;

ConVar cvWeaponCleanerDistance;
float g_fWeaponCleanerDistance;

/* Throwing Knifes */

ConVar cvThrowingKnifesDamage;
int g_iThrowingKnifesDamage;

ConVar cvThrowingKnifesDamageHeadshot;
int g_iThrowingKnifesDamageHeadshot;

/* Fireworks */

ConVar cvFireworksEnabled;
bool g_bFireworksEnabled;

/* Zombies */

ConVar cvZombieActivateDelay;
float g_fZombieActivateDelay;

ConVar cvZombieWaveInterval;
float g_fZombieWaveInterval;

ConVar cvZombieSpeedDefault;
float g_fZombieSpeedDefault;

ConVar cvZombieSpeedNear;
float g_fZombieSpeedNear;

ConVar cvZombieSpeedDistance;
float g_fZombieSpeedDistance;

ConVar cvZombieGravity;
float g_fZombieGravity;

ConVar cvZombieHealth;
int g_iZombieHealth;

ConVar cvZombieArmor;
int g_iZombieArmor;

ConVar cvZombieHeal;
int g_iZombieHeal;

ConVar cvZombieFOV;
int g_iZombieFOV;

ConVar cvZombieThirdperson;
bool g_bZombieThirdperson;

ConVar cvZombieEndgame;
bool g_bZombieEndgame;

ConVar cvZombieModel;
char g_sZombieModel[256];

ConVar cvZombieArmsModel;
char g_sZombieArmsModel[256];

/* Parachute */

ConVar cvParachuteModel;
char g_sParachuteModel[256];

ConVar cvParachuteFallspeedMax;
float fParachuteFallspeedMax;

ConVar cvParachuteAllowShooting;
bool bParachuteAllowShooting;

/* Voice */

ConVar cvVoiceTributeRange;
float g_fVoiceTributeRange;

ConVar cvVoiceImmunity;
int g_iVoiceImmunity;

/* Events */

ConVar cvEventDelay;
float g_fEventDelay;

ConVar cvEventEndgame;
bool g_bEventEndgame;

ConVar cvEventIntervalMin;
float g_fEventIntervalMin;

ConVar cvEventIntervalMax;
float g_fEventIntervalMax;

ConVar cvEventVoteTime;
float g_fEventVoteTime;

ConVar cvEventIonCannonEnable;
int g_iEventIonCannon;

ConVar cvEventIonCannonStartRange;
float g_fEventIonCannonStartRange;

ConVar cvEventIonCannonUpdateInterval;
float g_fEventIonCannonUpdateInterval;

ConVar cvEventNapalmEnable;
int g_iEventNapalm;

ConVar cvEventNapalmProjectiles;
int g_iEventNapalmProjectiles;

ConVar cvEventNapalmRadius;
float g_fEventNapalmRadius;

ConVar cvEventThunderStormEnable;
int g_iEventThunderStorm;

ConVar cvEventThunderStormDamage;
int g_iEventThunderStormDamage;

ConVar cvEventThunderStormLightnings;
int g_iEventThunderStormLightnings;

ConVar cvEventThunderStormInterval;
float g_fEventThunderStormInterval;

ConVar cvEventChargedWaterEnable;
int g_iEventChargedWater;

ConVar cvEventChargedWaterDamage;
int g_iEventChargedWaterDamage;

ConVar cvEventChargedWaterSimulationsMax;
int g_iEventChargedWaterSimulationsMax;

ConVar cvEventChargedWaterInterval;
float g_fEventChargedWaterInterval;

ConVar cvEventEarthquakeEnable;
int g_iEventEarthquake;

ConVar cvEventEarthquakeIntensity;
float g_fEventEarthquakeIntensity;

ConVar cvEventEarthquakeFrequency;
float g_fEventEarthquakeFrequency;

ConVar cvEventEarthquakeDuration;
float g_fEventEarthquakeDuration;

ConVar cvEventArtilerieEnable;
int g_iEventArtilerie;

ConVar cvEventArtilerieMagnitude;
int g_iEventArtilerieMagnitude;

ConVar cvEventArtillerieRadius;
float g_fEventArtillerieRadius;

ConVar cvEventArtillerieProjectiles;
int g_iEventArtillerieProjectiles;

/* Mine */

ConVar cvMineMagnitude;
int g_iMineMagnitude;

ConVar cvMineAbortExplode;
bool g_bMineAbortExplode;

/* Party */

ConVar cvPartyLimit;
int g_iPartyLimit;

ConVar cvPartyMaxDistance;
float g_fPartyMaxDistance;

ConVar cvPartyDmgMateAttacksMate;
float g_fPartyDmgMateAttacksMate;

ConVar cvPartyDmgPartyAttacksSingle;
float g_fPartyDmgPartyAttacksSingle;

ConVar cvPartyDmgPartyAttacksParty;
float g_fPartyDmgPartyAttacksParty;

ConVar cvPartyDmgSingleAttacksParty;
float g_fPartyDmgSingleAttacksParty;

ConVar cvPartyResetRoundstartEnabled;
bool g_bPartyResetRoundstart;

ConVar cvPartyResetEndgame;
int g_iPartyResetEndgame;

ConVar cvPartyVIP;
bool g_bPartyVIP;

/* Compass */

ConVar cvCompassDisorientationDistance;
float g_fCompassDisorientationDistance;

ConVar cvCompassShowNameDistance;
float g_fCompassShowNameDistance;

ConVar cvCompassAllowParty;
bool g_bCompassAllowParty;

/* TA Grenade */

ConVar cvTagrenadeVisible;
bool g_bTagrenadeVisible;

ConVar cvTagrenadeRange;
float g_fTagrenadeRange;

ConVar cvTagrenadeTime;
float g_fTagrenadeTime;

/* Points */

ConVar cvPointsKill;
int g_iPointsKill;

ConVar cvPointsKnifeKill;
int g_iPointsKnifeKill;

ConVar cvPointsWin;
int g_iPointsWin;

ConVar cvPointsZombieKill;
int g_iPointsZombieKill;

ConVar cvPointsKillAsZombie;
int g_iPointsKillAsZombie;

ConVar cvPointsPlaytime;
int g_iPointsPlaytime;

ConVar cvPointsDefuseMine;
int g_iPointsDefuseMine;

ConVar cvPointsEndgame;
int g_iPointsEndgame;

ConVar cvPointsFinalKnifefight;
int g_iPointsFinalKnifefight;

ConVar cvPointsVIP;
float g_fPointsVIP;

/* Shop */

ConVar cvShopSelfOnly;
bool g_bShopSelfOnly;

ConVar cvShopAlive;
bool g_bShopAlive;

ConVar cvShopDead;
bool g_bShopDead;

ConVar cvShopCmds;
char g_sShopCmds[256];
Handle g_hTrie_ShopCmds = null;

ConVar cvShopEventTime;
float g_fShopEventTime;

ConVar cvShopSpawnTime;
float g_fShopSpawnTime;

ConVar cvShopEvents;
bool g_bShopEvents;

ConVar cvShopEventPriceIonCannon;
int g_iShopEventPriceIonCannon;

ConVar cvShopEventPriceNapalm;
int g_iShopEventPriceNapalm;

ConVar cvShopEventPriceThunderStorm;
int g_iShopEventPriceThunderStorm;

ConVar cvShopEventPriceChargedWater;
int g_iShopEventPriceChargedWater;

ConVar cvShopEventPriceEarthquake;
int g_iShopEventPriceEarthquake;

ConVar cvShopEventPriceArtillery;
int g_iShopEventPriceArtillery;

ConVar cvShopBinocularsPrice;
int g_iShopBinocularsPrice;

ConVar cvShopCompassPrice;
int g_iShopCompassPrice;

ConVar cvShopTracerPrice;
int g_iShopTracerPrice;

ConVar cvShopJammerPrice;
int g_iShopJammerPrice;

ConVar cvShopGpsPrice;
int g_iShopGpsPrice;

ConVar cvShopParachutePrice;
int g_iShopParachutePrice;

/* Survival */

ConVar cvSurvival;
bool g_bSurvival;

ConVar g_cvSurvivalCheckInterval;
float g_fSurvivalCheckInterval;

/* Check Speed */

ConVar g_cvSpeedWalking;
float g_fSpeedWalking;

ConVar g_cvSpeedRunning;
float g_fSpeedRunning;

/* Hunger */

ConVar g_cvEnergyDefault;
float g_fEnergyDefault;

ConVar g_cvHungerDefault;
float g_fHungerDefault;

ConVar g_cvStarveCamping;
float g_fStarveCamping;

ConVar g_cvStarveWalking;
float g_fStarveWalking;

ConVar g_cvStarveRunning;
float g_fStarveRunning;

ConVar g_cvHungerCamping;
float g_fHungerCamping;

ConVar g_cvHungerWalking;
float g_fHungerWalking;

ConVar g_cvHungerRunning;
float g_fHungerRunning;

/* Health */

ConVar g_cvHealthStarvingPassage;
float g_fHealthStarvingPassage;

ConVar g_cvHealthRecoveryPassage;
float g_fHealthRecoveryPassage;

ConVar g_cvHealthRecoveryCost;
float g_fHealthRecoveryCost;

ConVar g_cvHealthMin;
int g_iHealthMin;

ConVar g_cvHealthMax;
int g_iHealthMax;

ConVar g_cvHealthGain;
float g_fHealthGain;

ConVar g_cvHealthLose;
float g_fHealthLose;

/* Thirst */

ConVar g_cvThirstDefault;
float g_fThirstDefault;

ConVar g_cvThirstCamping;
float g_fThirstCamping;

ConVar g_cvThirstWalking;
float g_fThirstWalking;

ConVar g_cvThirstRunning;
float g_fThirstRunning;

ConVar g_cvThirstWaterDrinkRate;
float g_fThirstWaterDrinkRate;

/* Stamina */

ConVar g_cvStaminaDefault;
float g_fStaminaDefault;

ConVar g_cvStaminaRunningCost;
float g_fStaminaRunningCost;

ConVar g_cvStaminaJumpCost;
float g_fStaminaJumpCost;

ConVar g_cvStaminaRecoverBuffer;
float g_fStaminaRecoverBuffer;

ConVar g_cvStaminaRecoverBase;
float g_fStaminaRecoverBase;

ConVar g_cvStaminaRecoverBasePerc;
float g_fStaminaRecoverBasePerc;

ConVar g_cvStaminaRecoverThirstPerc;
float g_fStaminaRecoverThirstPerc;

ConVar g_cvStaminaRecoverVIP;
float g_fStaminaRecoverVIP;

/* Speed */

ConVar g_cvStaminaPerfectSpeed;
float g_fStaminaPerfectSpeed;

ConVar g_cvStaminaGoodSpeed;
float g_fStaminaGoodSpeed;

ConVar g_cvStaminaLowSpeed;
float g_fStaminaLowSpeed;

ConVar g_cvStaminaVeryLowSpeed;
float g_fStaminaVerySpeed;

ConVar g_cvStaminaEmptySpeed;
float g_fStaminaEmptySpeed;

/* Knockout */

ConVar cvKnockoutFake;
bool g_bKnockoutFake;

ConVar g_cvKnockoutChance;
float g_fKnockoutChance;

ConVar g_cvKnockoutDamage;
int g_iKnockoutDamage;

ConVar g_cvKnockoutTimeMin;
float g_fKnockoutTimeMin;

ConVar g_cvKnockoutTimeMax
float g_fKnockoutTimeMax;

ConVar cvFakedeathCmds;
char g_sFakedeathCmds[256];
Handle g_hTrie_FakedeathCmds = null;

ConVar cvSuicideCmds;
char g_sSuicideCmds[256];
Handle g_hTrie_SuicideCmds = null;

/* Stats */

ConVar cvStatsCmds;
char g_sStatsCmds[256];
Handle g_hTrie_StatsCmds = null;

ConVar cvTopCmds;
char g_sTopCmds[256];
Handle g_hTrie_TopCmds = null;

ConVar cvStatsMeShowAll;
bool g_bStatsMeShowAll;

ConVar cvStatsMeMessages;
int g_iStatsMeMessages;

ConVar cvTopLimit;
int g_iTopLimit;

/* Sounds */

ConVar cvSndWelcome;
bool g_bSndWelcome;
char sndWelcome[256];

ConVar cvSndRoundstart;
bool g_bSndRoundstart;
char sndRoundStart[256];

ConVar cvSndRoundstart2;
bool g_bSndRoundstart2;
char sndRoundStart2[256];

ConVar cvSndRoundDraw;
bool g_bSndRoundDraw;
char sndRoundDraw[256];

ConVar cvSndCannon;
bool g_bSndCannon;
char sndCannon[256];

ConVar cvSndEndgame;
bool g_bSndEndgame;
char sndEndgame[256];

ConVar cvSndBeacon;
bool g_bSndBeacon;
char sndBeacon[256];

ConVar cvSndKnifeFight;
bool g_bSndKnifeFight;
char sndKnifeFight[256];

ConVar cvSndWave;
bool g_bSndWave;
char sndWave[256];

ConVar cvSndFireworks1;
bool g_bSndFireworks1;
char sndFireworks1[256];

ConVar cvSndFireworks2;
bool g_bSndFireworks2;
char sndFireworks2[256];

ConVar cvSndFireworks3;
bool g_bSndFireworks3;
char sndFireworks3[256];

ConVar cvSndFireworks4;
bool g_bSndFireworks4;
char sndFireworks4[256];

ConVar cvSndIonApproaching;
bool g_bSndIonApproaching;
char sndIonApproaching[256];

ConVar cvSndIonCharging;
bool g_bSndIonCharging;
char sndIonCharging[256];

ConVar cvSndIonDeployed;
bool g_bSndIonDeployed;
char sndIonDeployed[256];

ConVar cvSndIonImplosion;
bool g_bSndIonImplosion;
char sndIonImplosion[256];

ConVar cvSndEarthquake;
bool g_bSndEarthquake;
char sndEarthquake[256];

/* VIP */

ConVar cvVIPflags;
int g_iVIPflags;

/* Loot */

ConVar g_cvLoopWipeInterval;
float g_fLoopWipeInterval;

char sndCountdown[][] = 
{
	"hg/zombie/countdown/1.mp3",
	"hg/zombie/countdown/2.mp3",
	"hg/zombie/countdown/3.mp3",
	"hg/zombie/countdown/4.mp3",
	"hg/zombie/countdown/5.mp3",
	"hg/zombie/countdown/6.mp3",
	"hg/zombie/countdown/7.mp3",
	"hg/zombie/countdown/8.mp3",
	"hg/zombie/countdown/9.mp3",
	"hg/zombie/countdown/10.mp3"
}

char sndThunderStorm[][] = 
{
	"ambient/weather/thunder1.wav", 
	"ambient/weather/thunder2.wav", 
	"ambient/weather/thunder3.wav", 
	"ambient/playonce/weather/thunder4.wav",
	"ambient/playonce/weather/thunder5.wav", 
	"ambient/playonce/weather/thunder6.wav"
};

/* Other */

ConVar cvImmunityAlpha;

/* Materials */

int g_iBombRing;
int g_iLaser;
int g_iHalo;
int g_iFireSmoke;
int g_iBlueGlow;

void CreateConVars()
{
	CreateConVar("hg_version", PLUGIN_VERSION, "Hunger-Games version", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	cvPrefix = CreateConVar("hg_prefix", "{lightred}[HG]{lime}", "Chat prefix.");
	GetConVarString(cvPrefix, g_sPrefix, sizeof(g_sPrefix));
	HookConVarChange(cvPrefix, OnSettingChanged);
	
	g_cvEndgameMaxPlayers = CreateConVar("hg_endgame_max_players", "5", "Max players alive to start endgame.");
	g_iEndgameMaxPlayers = GetConVarInt(g_cvEndgameMaxPlayers);
	HookConVarChange(g_cvEndgameMaxPlayers, OnSettingChanged);
	
	g_cvEndgameMinDelay = CreateConVar("hg_endgame_min_delay", "200.0", "Min time from round start to allow endgame being started.");
	g_fEndgameMinDelay = GetConVarFloat(g_cvEndgameMinDelay);
	HookConVarChange(g_cvEndgameMinDelay, OnSettingChanged);
	
	g_cvCheckInterval = CreateConVar("hg_check_interval", "0.5", "Delay between player status checks.");
	g_fCheckInterval = GetConVarFloat(g_cvCheckInterval);
	HookConVarChange(g_cvCheckInterval, OnSettingChanged);
	
	g_cvRoundEndDelay = CreateConVar("hg_roundend_delay", "15.0", "Delay before restarting the round.");
	g_fRoundEndDelay = GetConVarFloat(g_cvRoundEndDelay);
	HookConVarChange(g_cvRoundEndDelay, OnSettingChanged);
	
	g_cvDeathDelay = CreateConVar("hg_death_delay", "30.0", "Delay before showing the death message(s) and playing the cannon sound(s). 0.0 Will switch to old method.");
	g_fDeathDelay = GetConVarFloat(g_cvDeathDelay);
	HookConVarChange(g_cvDeathDelay, OnSettingChanged);
	
	cvDeathTeamChange = CreateConVar("hg_death_team_change", "1", "Change team when delayed death announced.");
	g_bDeathTeamChange = GetConVarBool(cvDeathTeamChange);
	HookConVarChange(cvDeathTeamChange, OnSettingChanged);
	
	/* Give Items Delayed */
	
	cvGiveKnifeDelay = CreateConVar("hg_give_knife_delay", "180.0", "Give player a knife X sec until spawn when they don't have one (-1.0: Disable).");
	g_fGiveKnifeDelay = GetConVarFloat(cvGiveKnifeDelay);
	HookConVarChange(cvGiveKnifeDelay, OnSettingChanged);
	
	cvGiveBinocularsDelay = CreateConVar("hg_give_binoculars_delay", "60.0", "Give player binoculars X sec until spawn when they don't have one (-1.0: Disable).");
	g_fGiveBinocularsDelay = GetConVarFloat(cvGiveBinocularsDelay);
	HookConVarChange(cvGiveBinocularsDelay, OnSettingChanged);
	
	cvGiveCompassDelay = CreateConVar("hg_give_compass_delay", "120.0", "Give player a compass X sec until spawn when they don't have one (-1.0: Disable).");
	g_fGiveCompassDelay = GetConVarFloat(cvGiveCompassDelay);
	HookConVarChange(cvGiveCompassDelay, OnSettingChanged);
	
	cvGiveParachuteDelay = CreateConVar("hg_give_parachute_delay", "180.0", "Give player a parachute X sec until spawn when they don't have one (-1.0: Disable).");
	g_fGiveParachuteDelay = GetConVarFloat(cvGiveParachuteDelay);
	HookConVarChange(cvGiveParachuteDelay, OnSettingChanged);

	/* HUD */
	
	cvHudTickInterval = CreateConVar("hg_hud_interval", "0.5", "Update hud every X.x seconds.");
	g_fHudTickInterval = GetConVarFloat(cvHudTickInterval);
	HookConVarChange(cvHudTickInterval, OnSettingChanged);
	
	cvHudMaxTicks = CreateConVar("hg_hud_max_ticks", "15", "Amount of ticks to cycle hud. Survival hud is default and shown for the rest amount of ticks and when other huds have no info.");
	g_iHudMaxTicks = GetConVarInt(cvHudMaxTicks);
	HookConVarChange(cvHudMaxTicks, OnSettingChanged);
	
	cvHudEquipmentTicks = CreateConVar("hg_hud_equip_ticks", "3", "Amount of ticks to show equipment.");
	g_iHudEquipmentTicks = GetConVarInt(cvHudEquipmentTicks);
	HookConVarChange(cvHudEquipmentTicks, OnSettingChanged);
	
	cvHudCompassTicks = CreateConVar("hg_hud_compass_ticks", "3", "Show compass every X HUD ticks.");
	g_iHudCompassTicks = GetConVarInt(cvHudCompassTicks);
	HookConVarChange(cvHudCompassTicks, OnSettingChanged);

	/* Endgame */
	
	cvBeaconInterval = CreateConVar("hg_beacon_interval", "2.5", "Endgame beacon interval (-1.0: Disable).");
	g_fBeaconInterval = GetConVarFloat(cvBeaconInterval);
	HookConVarChange(cvBeaconInterval, OnSettingChanged);
	
	cvKnifeFightDelay = CreateConVar("hg_knife_fight_delay", "90.0", "If beacon is not enough start final knife fight after this amount of this (-1.0: Disable).");
	g_fKnifeFightDelay = GetConVarFloat(cvKnifeFightDelay);
	HookConVarChange(cvKnifeFightDelay, OnSettingChanged);
	
	cvEndgameWipeLoot = CreateConVar("hg_endgame_wipe_loot", "0", "If enabled deletes all spawned loot when endgame starts.");
	g_bEndgameWipeLoot = GetConVarBool(cvEndgameWipeLoot);
	HookConVarChange(cvEndgameWipeLoot, OnSettingChanged);
		
	/* Weapon Cleaner */
	
	cvWeaponCleanerInterval = CreateConVar("hg_weapon_cleaner_interval", "10.0", "Interval to delete weapons on ground (-1.0: Disable).");
	g_fWeaponCleanerInterval = GetConVarFloat(cvWeaponCleanerInterval);
	HookConVarChange(cvWeaponCleanerInterval, OnSettingChanged);
	
	cvWeaponCleanerDistance = CreateConVar("hg_weapon_cleaner_distance", "2048.0", "If a player is within this range don't cleanup weapons (-1.0: Disable).");
	g_fWeaponCleanerDistance = GetConVarFloat(cvWeaponCleanerDistance);
	HookConVarChange(cvWeaponCleanerDistance, OnSettingChanged);
	
	/* Throwing Knifes */
	
	cvThrowingKnifesDamage = CreateConVar("hg_throwing_knifes_damage", "37", "Throwing knifes damage.");
	g_iThrowingKnifesDamage = GetConVarInt(cvThrowingKnifesDamage);
	HookConVarChange(cvThrowingKnifesDamage, OnSettingChanged);
	
	cvThrowingKnifesDamageHeadshot = CreateConVar("hg_throwing_knifes_damage_hs", "87", "Throwing knifes damage.");
	g_iThrowingKnifesDamageHeadshot = GetConVarInt(cvThrowingKnifesDamageHeadshot);
	HookConVarChange(cvThrowingKnifesDamageHeadshot, OnSettingChanged);

	/* Fireworks */
	
	cvFireworksEnabled = CreateConVar("hg_fireworks_enable", "1", "Enable fireworks above winners head.");
	g_bFireworksEnabled = GetConVarBool(cvFireworksEnabled);
	HookConVarChange(cvFireworksEnabled, OnSettingChanged);

	/* Zombies */
	
	cvZombieActivateDelay = CreateConVar("hg_zombie_activate_delay", "35.0", "Delay between first player death and first zombie spawn wave announcement (-1.0: Disable).");
	g_fZombieActivateDelay = GetConVarFloat(cvZombieActivateDelay);
	HookConVarChange(cvZombieActivateDelay, OnSettingChanged);
	
	cvZombieWaveInterval = CreateConVar("hg_zombie_wave_interval", "45.0", "Delay between zombie respawn waves.");
	g_fZombieWaveInterval = GetConVarFloat(cvZombieWaveInterval);
	HookConVarChange(cvZombieWaveInterval, OnSettingChanged);
	
	cvZombieSpeedDefault = CreateConVar("hg_zombie_speed", "1.35", "Default zombie speed.");
	g_fZombieSpeedDefault = GetConVarFloat(cvZombieSpeedDefault);
	HookConVarChange(cvZombieSpeedDefault, OnSettingChanged);
	
	cvZombieSpeedNear = CreateConVar("hg_zombie_speed_near", "0.85", "Zombie speed if tributes are in range.");
	g_fZombieSpeedNear = GetConVarFloat(cvZombieSpeedNear);
	HookConVarChange(cvZombieSpeedNear, OnSettingChanged);
	
	cvZombieSpeedDistance = CreateConVar("hg_zombie_speed_dist", "384.0", "Distance where to switch zombie speed (-1.0: Disable).");
	g_fZombieSpeedDistance = GetConVarFloat(cvZombieSpeedDistance);
	HookConVarChange(cvZombieSpeedDistance, OnSettingChanged);
	
	cvZombieGravity = CreateConVar("hg_zombie_gravity", "0.84", "Zombie gravity (1.0: Disable).");
	g_fZombieGravity = GetConVarFloat(cvZombieGravity);
	HookConVarChange(cvZombieGravity, OnSettingChanged);
	
	cvZombieHealth = CreateConVar("hg_zombie_health", "200", "Zombie spawn health.");
	g_iZombieHealth = GetConVarInt(cvZombieHealth);
	HookConVarChange(cvZombieHealth, OnSettingChanged);
	
	cvZombieArmor = CreateConVar("hg_zombie_armor", "0", "Zombie spawn armor.");
	g_iZombieArmor = GetConVarInt(cvZombieArmor);
	HookConVarChange(cvZombieArmor, OnSettingChanged);
	
	cvZombieHeal = CreateConVar("hg_zombie_heal", "0", "Heal zombies up to this amount when they are still alive but new wave is spawned (0: Disable).");
	g_iZombieHeal = GetConVarInt(cvZombieHeal);
	HookConVarChange(cvZombieHeal, OnSettingChanged);
	
	cvZombieFOV = CreateConVar("hg_zombie_fov", "110", "Zombie field of view (90: Default).");
	g_iZombieFOV = GetConVarInt(cvZombieFOV);
	HookConVarChange(cvZombieFOV, OnSettingChanged);
	
	cvZombieEndgame = CreateConVar("hg_zombie_endgame", "1", "Don't slay zombies in endgame when endgame starts, but stops respawn waves anyway.");
	g_bZombieEndgame = GetConVarBool(cvZombieEndgame);
	HookConVarChange(cvZombieEndgame, OnSettingChanged);
	
	cvZombieThirdperson = CreateConVar("hg_zombie_thirdperson", "1", "Use thridperson view for zombies.");
	g_bZombieThirdperson = GetConVarBool(cvZombieThirdperson);
	HookConVarChange(cvZombieThirdperson, OnSettingChanged);
	
	cvZombieModel = CreateConVar("hg_zombie_model", "models/player/custom/fleshpound/fleshpound.mdl", "Path to the zombie model.");
	GetConVarString(cvZombieModel, g_sZombieModel, sizeof(g_sZombieModel));
	HookConVarChange(cvZombieModel, OnSettingChanged);
	
	cvZombieArmsModel = CreateConVar("hg_zombie_arms_model", "", "Path to the zombie arms model.");
	GetConVarString(cvZombieArmsModel, g_sZombieArmsModel, sizeof(g_sZombieArmsModel));
	HookConVarChange(cvZombieArmsModel, OnSettingChanged);
	
	/* Parachute */
	
	cvParachuteModel = CreateConVar("hg_parachute_model", "models/parachute/parachute_carbon.mdl", "Path to the parachute model.");
	GetConVarString(cvParachuteModel, g_sParachuteModel, sizeof(g_sParachuteModel));
	HookConVarChange(cvParachuteModel, OnSettingChanged);
	
	cvParachuteFallspeedMax = CreateConVar("hg_parachute_fallspeed_max", "100.0", "Max fall speed while using parachute.");
	fParachuteFallspeedMax = GetConVarFloat(cvParachuteFallspeedMax);
	HookConVarChange(cvParachuteFallspeedMax, OnSettingChanged);
	
	cvParachuteAllowShooting = CreateConVar("hg_parachute_shoot", "0", "Allow shooting while using parachute (0 to disable).");
	bParachuteAllowShooting = GetConVarBool(cvParachuteAllowShooting);
	HookConVarChange(cvParachuteAllowShooting, OnSettingChanged);
	
	/* Voice */
	
	cvVoiceTributeRange = CreateConVar("hg_voice_tribute_range", "2048.0", "Max. distance between alive tributes to hear each other (-1.0: Any range).");
	g_fVoiceTributeRange = GetConVarFloat(cvVoiceTributeRange);
	HookConVarChange(cvVoiceTributeRange, OnSettingChanged);
	
	cvVoiceImmunity = CreateConVar("hg_voice_immunity", "10", "Immunity level for ZCore-Voice.");
	g_iVoiceImmunity = GetConVarInt(cvVoiceImmunity);
	HookConVarChange(cvVoiceImmunity, OnSettingChanged);
	
	/* Events */
	
	cvEventDelay = CreateConVar("hg_event_delay", "120.0", "Delay from round start to first event (-1.0: Disable Events).");
	g_fEventDelay = GetConVarFloat(cvEventDelay);
	HookConVarChange(cvEventDelay, OnSettingChanged);
	
	cvEventEndgame = CreateConVar("hg_event_endgame", "1", "Allow events in endgame.");
	g_bEventEndgame = GetConVarBool(cvEventEndgame);
	HookConVarChange(cvEventEndgame, OnSettingChanged);
	
	cvEventIntervalMin = CreateConVar("hg_event_interval_min", "30.0", "Min delay between events.");
	g_fEventIntervalMin = GetConVarFloat(cvEventIntervalMin);
	HookConVarChange(cvEventIntervalMin, OnSettingChanged);
	
	cvEventIntervalMax = CreateConVar("hg_event_interval_max", "40.0", "Max delay between events.");
	g_fEventIntervalMax = GetConVarFloat(cvEventIntervalMax);
	HookConVarChange(cvEventIntervalMax, OnSettingChanged);
	
	cvEventVoteTime = CreateConVar("hg_event_vote_time", "-1.0", "Amount of time to allow voting for dead players (-1.0: Random).");
	g_fEventVoteTime = GetConVarFloat(cvEventVoteTime);
	HookConVarChange(cvEventVoteTime, OnSettingChanged);
	
	cvEventIonCannonEnable = CreateConVar("hg_event_ioncannon_enable", "0", "Enable ion cannon event.");
	g_iEventIonCannon = GetConVarInt(cvEventIonCannonEnable);
	HookConVarChange(cvEventIonCannonEnable, OnSettingChanged);
	
	cvEventIonCannonStartRange = CreateConVar("hg_event_ioncannon_range", "650.0", "Range of the ion cannon event.");
	g_fEventIonCannonStartRange = GetConVarFloat(cvEventIonCannonStartRange);
	HookConVarChange(cvEventIonCannonStartRange, OnSettingChanged);
	
	cvEventIonCannonUpdateInterval = CreateConVar("hg_event_ioncannon_update_interval", "0.1", "Update interval of the ion cannon event.");
	g_fEventIonCannonUpdateInterval = GetConVarFloat(cvEventIonCannonUpdateInterval);
	HookConVarChange(cvEventIonCannonUpdateInterval, OnSettingChanged);
	
	cvEventNapalmEnable = CreateConVar("hg_event_napalm_enable", "1", "Enable napalm event.");
	g_iEventNapalm = GetConVarInt(cvEventNapalmEnable);
	HookConVarChange(cvEventNapalmEnable, OnSettingChanged);
	
	cvEventNapalmProjectiles = CreateConVar("hg_event_napalm_projectiles", "20", "How much napalm projectiles are spawned for the napalm air strike event.");
	g_iEventNapalmProjectiles = GetConVarInt(cvEventNapalmProjectiles);
	HookConVarChange(cvEventNapalmProjectiles, OnSettingChanged);
	
	cvEventNapalmRadius = CreateConVar("hg_event_napalm_radius", "1024.0", "Radius/Spread of the napalm air strike.");
	g_fEventNapalmRadius = GetConVarFloat(cvEventNapalmRadius);
	HookConVarChange(cvEventNapalmRadius, OnSettingChanged);
	
	cvEventThunderStormEnable = CreateConVar("hg_event_thunderstorm_enable", "1", "Enable thunder storm event.");
	g_iEventThunderStorm = GetConVarInt(cvEventThunderStormEnable);
	HookConVarChange(cvEventThunderStormEnable, OnSettingChanged);
	
	cvEventThunderStormDamage = CreateConVar("hg_event_thunderstorm_damage", "25", "Thunder storm damage per lightning.");
	g_iEventThunderStormDamage = GetConVarInt(cvEventThunderStormDamage);
	HookConVarChange(cvEventThunderStormDamage, OnSettingChanged);
	
	cvEventThunderStormLightnings = CreateConVar("hg_event_thunderstorm_lightnings", "5", "Amount of thunder storm lightnings.");
	g_iEventThunderStormLightnings = GetConVarInt(cvEventThunderStormLightnings);
	HookConVarChange(cvEventThunderStormLightnings, OnSettingChanged);
	
	cvEventThunderStormInterval = CreateConVar("hg_event_thunderstorm_interval", "1.0", "Interval of the thunder storm event.");
	g_fEventThunderStormInterval = GetConVarFloat(cvEventThunderStormInterval);
	HookConVarChange(cvEventThunderStormInterval, OnSettingChanged);
	
	cvEventChargedWaterEnable = CreateConVar("hg_event_chargedwater_enable", "1", "Enable charged water event.");
	g_iEventChargedWater = GetConVarInt(cvEventChargedWaterEnable);
	HookConVarChange(cvEventChargedWaterEnable, OnSettingChanged);
	
	cvEventChargedWaterDamage = CreateConVar("hg_event_chargedwater_damage", "3", "Damage for each interval of the charged water event.");
	g_iEventChargedWaterDamage = GetConVarInt(cvEventChargedWaterDamage);
	HookConVarChange(cvEventChargedWaterDamage, OnSettingChanged);
	
	cvEventChargedWaterSimulationsMax = CreateConVar("hg_event_chargedwater_max", "30", "How much times the charged water event deals damage.");
	g_iEventChargedWaterSimulationsMax = GetConVarInt(cvEventChargedWaterSimulationsMax);
	HookConVarChange(cvEventChargedWaterSimulationsMax, OnSettingChanged);
	
	cvEventChargedWaterInterval = CreateConVar("hg_event_chargedwater_interval", "0.5", "Interval of the charged water event.");
	g_fEventChargedWaterInterval = GetConVarFloat(cvEventChargedWaterInterval);
	HookConVarChange(cvEventChargedWaterInterval, OnSettingChanged);
	
	cvEventEarthquakeEnable = CreateConVar("hg_event_earthquake_enable", "1", "Enable earthquake event.");
	g_iEventEarthquake = GetConVarInt(cvEventEarthquakeEnable);
	HookConVarChange(cvEventEarthquakeEnable, OnSettingChanged);
	
	cvEventEarthquakeIntensity = CreateConVar("hg_event_earthquake_intensity", "25.0", "Shake intensity of the earthquake event.");
	g_fEventEarthquakeIntensity = GetConVarFloat(cvEventEarthquakeIntensity);
	HookConVarChange(cvEventEarthquakeIntensity, OnSettingChanged);
	
	cvEventEarthquakeFrequency = CreateConVar("hg_event_earthquake_frequency", "250.0", "Shake frequency of the earthquake event.");
	g_fEventEarthquakeFrequency = GetConVarFloat(cvEventEarthquakeFrequency);
	HookConVarChange(cvEventEarthquakeFrequency, OnSettingChanged);
	
	cvEventEarthquakeDuration = CreateConVar("hg_event_earthquake_duration", "10.0", "Duration of the earthquake event.");
	g_fEventEarthquakeDuration = GetConVarFloat(cvEventEarthquakeDuration);
	HookConVarChange(cvEventEarthquakeDuration, OnSettingChanged);
	
	cvEventArtilerieEnable = CreateConVar("hg_event_artilerie_enable", "1", "Enable artilerie event.");
	g_iEventArtilerie = GetConVarInt(cvEventArtilerieEnable);
	HookConVarChange(cvEventArtilerieEnable, OnSettingChanged);
	
	cvEventArtilerieMagnitude = CreateConVar("hg_event_artilerie_maginitude", "150", "Magnitute of artilerie event.");
	g_iEventArtilerieMagnitude = GetConVarInt(cvEventArtilerieMagnitude);
	HookConVarChange(cvEventArtilerieMagnitude, OnSettingChanged);
	
	cvEventArtillerieRadius = CreateConVar("hg_event_artilerie_range", "1024.0", "Radius of the artilerie event.");
	g_fEventArtillerieRadius = GetConVarFloat(cvEventArtillerieRadius);
	HookConVarChange(cvEventArtillerieRadius, OnSettingChanged);
	
	cvEventArtillerieProjectiles = CreateConVar("hg_event_artilerie_projectiles", "20", "Amount of projectiles of artilerie event.");
	g_iEventArtillerieProjectiles = GetConVarInt(cvEventArtillerieProjectiles);
	HookConVarChange(cvEventArtillerieProjectiles, OnSettingChanged);
	
	/* Proxy Mine */
	
	cvMineMagnitude = CreateConVar("hg_mine_magnitude", "95", "Magnitude of proxy mines.");
	g_iMineMagnitude = GetConVarInt(cvMineMagnitude);
	HookConVarChange(cvMineMagnitude, OnSettingChanged);
	
	cvMineAbortExplode = CreateConVar("hg_mine_abort_explode", "1", "Let proxy mines explode when abort defusing.");
	g_bMineAbortExplode = GetConVarBool(cvMineAbortExplode);
	HookConVarChange(cvMineAbortExplode, OnSettingChanged);

	/* Party */
	
	cvPartyLimit = CreateConVar("hg_party_limit", "3", "How much players can be in the same district (Values lower than 2 will disable the party system).");
	g_iPartyLimit = GetConVarInt(cvPartyLimit);
	HookConVarChange(cvPartyLimit, OnSettingChanged);
	
	cvPartyMaxDistance = CreateConVar("hg_party_max_distance", "300.0", "Max distance between 2 players to make a party. (0.0: No Limit)");
	g_fPartyMaxDistance = GetConVarFloat(cvPartyMaxDistance);
	HookConVarChange(cvPartyMaxDistance, OnSettingChanged);
	
	cvPartyDmgMateAttacksMate = CreateConVar("hg_party_dmg_mate_attacks_mate", "0.7", "Damage from party members of the same party.");
	g_fPartyDmgMateAttacksMate = GetConVarFloat(cvPartyDmgMateAttacksMate);
	HookConVarChange(cvPartyDmgMateAttacksMate, OnSettingChanged);
	
	cvPartyDmgPartyAttacksSingle = CreateConVar("hg_party_dmg_party_attacks_single", "0.95", "Damage from party members to non-party members.");
	g_fPartyDmgPartyAttacksSingle = GetConVarFloat(cvPartyDmgPartyAttacksSingle);
	HookConVarChange(cvPartyDmgPartyAttacksSingle, OnSettingChanged);
	
	cvPartyDmgPartyAttacksParty = CreateConVar("hg_party_dmg_party_attacks_party", "1.1", "Damage from party members to other party members.");
	g_fPartyDmgPartyAttacksParty = GetConVarFloat(cvPartyDmgPartyAttacksParty);
	HookConVarChange(cvPartyDmgPartyAttacksParty, OnSettingChanged);
	
	cvPartyDmgSingleAttacksParty = CreateConVar("hg_party_dmg_single_attacks_party", "1.1", "Damage from non-party members to party members.");
	g_fPartyDmgSingleAttacksParty = GetConVarFloat(cvPartyDmgSingleAttacksParty);
	HookConVarChange(cvPartyDmgSingleAttacksParty, OnSettingChanged);
	
	cvPartyResetRoundstartEnabled = CreateConVar("hg_party_roundstart_reset", "1", "Reset districts on roundstart.");
	g_bPartyResetRoundstart = GetConVarBool(cvPartyResetRoundstartEnabled);
	HookConVarChange(cvPartyResetRoundstartEnabled, OnSettingChanged);
	
	cvPartyResetEndgame = CreateConVar("hg_party_endgame_reset", "0", "Reset districts on endgame start (0: Disable glow, 1: Disable glow+icon, 2: Disable completly).");
	g_iPartyResetEndgame = GetConVarInt(cvPartyResetEndgame);
	HookConVarChange(cvPartyResetEndgame, OnSettingChanged);
	
	cvPartyVIP = CreateConVar("hg_party_vip", "0", "If set to 1 at least one players has to be vip to make form a party.");
	g_bPartyVIP = GetConVarBool(cvPartyVIP);
	HookConVarChange(cvPartyVIP, OnSettingChanged);

	/* Compass */
	
	cvCompassDisorientationDistance = CreateConVar("hg_compass_disorientation_distance", "1024.0", "If nearest player is closer than this use 4 instead of 8 directions in compass HUD.");
	g_fCompassDisorientationDistance = GetConVarFloat(cvCompassDisorientationDistance);
	HookConVarChange(cvCompassDisorientationDistance, OnSettingChanged);
	
	cvCompassShowNameDistance = CreateConVar("hg_compass_show_name_distance", "512.0", "Max distance to show name in compass HUD.");
	g_fCompassShowNameDistance = GetConVarFloat(cvCompassShowNameDistance);
	HookConVarChange(cvCompassShowNameDistance, OnSettingChanged);
	
	cvCompassAllowParty = CreateConVar("hg_compass_allow_party", "1", "Allow compass for players within a party.");
	g_bCompassAllowParty = GetConVarBool(cvCompassAllowParty);
	HookConVarChange(cvCompassAllowParty, OnSettingChanged);

	/* TA Grenade */
	
	cvTagrenadeVisible = CreateConVar("hg_tagrenade_visible", "1", "Use visible check for TA grenades.");
	g_bTagrenadeVisible = GetConVarBool(cvTagrenadeVisible);
	HookConVarChange(cvTagrenadeVisible, OnSettingChanged);
	
	cvTagrenadeRange = CreateConVar("hg_tagrenade_range", "1500.0", "Sets the proximity in which the tactical grenade will tag an opponent.");
	g_fTagrenadeRange = GetConVarFloat(cvTagrenadeRange);
	HookConVarChange(cvTagrenadeRange, OnSettingChanged);
	
	cvTagrenadeTime = CreateConVar("hg_tagrenade_time", "3.5", "How long a player is tagged for in seconds.");
	g_fTagrenadeTime = GetConVarFloat(cvTagrenadeTime);
	HookConVarChange(cvTagrenadeTime, OnSettingChanged);

	/* Points */
	
	cvPointsKill = CreateConVar("hg_points_kill", "50", "How much points a tribute gets for killing another tribute.");
	g_iPointsKill = GetConVarInt(cvPointsKill);
	HookConVarChange(cvPointsKill, OnSettingChanged);
	
	cvPointsKnifeKill = CreateConVar("hg_points_kill_knife", "75", "How much points a tribute gets for killing another tribute with a knife.");
	g_iPointsKnifeKill = GetConVarInt(cvPointsKnifeKill);
	HookConVarChange(cvPointsKnifeKill, OnSettingChanged);
	
	cvPointsWin = CreateConVar("hg_points_win", "150", "How much points a tribute gets for winning the round.");
	g_iPointsWin = GetConVarInt(cvPointsWin);
	HookConVarChange(cvPointsWin, OnSettingChanged);
	
	cvPointsZombieKill = CreateConVar("hg_points_kill_zombie", "15", "How much points a tribute gets for killing a zombie.");
	g_iPointsZombieKill = GetConVarInt(cvPointsZombieKill);
	HookConVarChange(cvPointsZombieKill, OnSettingChanged);
	
	cvPointsKillAsZombie = CreateConVar("hg_points_zombie_kill", "65", "How much points a zombie gets for killing a tribute.");
	g_iPointsKillAsZombie = GetConVarInt(cvPointsKillAsZombie);
	HookConVarChange(cvPointsKillAsZombie, OnSettingChanged);
	
	cvPointsPlaytime = CreateConVar("hg_points_playtime", "2", "How much points a player gets per minute.");
	g_iPointsPlaytime = GetConVarInt(cvPointsPlaytime);
	HookConVarChange(cvPointsPlaytime, OnSettingChanged);
	
	cvPointsDefuseMine = CreateConVar("hg_points_defuse", "15", "How much points a tribute gets for defusing a proxy mine.");
	g_iPointsDefuseMine = GetConVarInt(cvPointsDefuseMine);
	HookConVarChange(cvPointsDefuseMine, OnSettingChanged);
	
	cvPointsEndgame = CreateConVar("hg_points_endgame", "35", "How much points a tribute gets for being alive when endgame starts.");
	g_iPointsEndgame = GetConVarInt(cvPointsEndgame);
	HookConVarChange(cvPointsEndgame, OnSettingChanged);
	
	cvPointsFinalKnifefight = CreateConVar("hg_points_final", "50", "How much points a tribute gets for being alive when final knife fight starts.");
	g_iPointsFinalKnifefight = GetConVarInt(cvPointsFinalKnifefight);
	HookConVarChange(cvPointsFinalKnifefight, OnSettingChanged);
	
	cvPointsVIP = CreateConVar("hg_points_vip", "1.0", "Gives vip players more points. (1.5 = 150%).");
	g_fPointsVIP = GetConVarFloat(cvPointsVIP);
	HookConVarChange(cvPointsVIP, OnSettingChanged);
	
	/* Shop */
	
	cvShopSelfOnly = CreateConVar("hg_shop_self_only", "0", "If enabled players can only buy for themselves.");
	g_bShopSelfOnly = GetConVarBool(cvShopSelfOnly);
	HookConVarChange(cvShopSelfOnly, OnSettingChanged);
	
	cvShopAlive = CreateConVar("hg_shop_alive", "1", "Enable the sponsor shop for alive tributes.");
	g_bShopAlive = GetConVarBool(cvShopAlive);
	HookConVarChange(cvShopAlive, OnSettingChanged);
	
	cvShopDead = CreateConVar("hg_shop_dead", "1", "Enable the sponsor shop for dead players too.");
	g_bShopDead = GetConVarBool(cvShopDead);
	HookConVarChange(cvShopDead, OnSettingChanged);
	
	cvShopCmds = CreateConVar("hg_shop_cmds", "!shop;!sponsor;!buy;shop;buy:sponsor", "Sponsor shop commands seperated by \";\" (max 8 commands).");
	GetConVarString(cvShopCmds, g_sShopCmds, sizeof(g_sShopCmds));
	HookConVarChange(cvShopCmds, OnSettingChanged);
	GetShopCommands();
	
	cvShopEventTime = CreateConVar("hg_shop_eventtime", "30.0", "Min time to allow buying a new event.");
	g_fShopEventTime = GetConVarFloat(cvShopEventTime);
	HookConVarChange(cvShopEventTime, OnSettingChanged);
	
	cvShopSpawnTime = CreateConVar("hg_shop_spawntime", "60.0", "Enabled the sponsor shop this time after round start.");
	g_fShopSpawnTime = GetConVarFloat(cvShopSpawnTime);
	HookConVarChange(cvShopSpawnTime, OnSettingChanged);
	
	cvShopEvents = CreateConVar("hg_shop_events", "1", "Enable events category in sponsor shop.");
	g_bShopEvents = GetConVarBool(cvShopEvents);
	HookConVarChange(cvShopEvents, OnSettingChanged);
	
	cvShopEventPriceIonCannon = CreateConVar("hg_shop_event_ioncannon", "20000", "Price for buying an ion cannon event (0: Disable).");
	g_iShopEventPriceIonCannon = GetConVarInt(cvShopEventPriceIonCannon);
	HookConVarChange(cvShopEventPriceIonCannon, OnSettingChanged);
	
	cvShopEventPriceNapalm = CreateConVar("hg_shop_event_napalm", "5000", "Price for buying an napalm air strike event (0: Disable).");
	g_iShopEventPriceNapalm = GetConVarInt(cvShopEventPriceNapalm);
	HookConVarChange(cvShopEventPriceNapalm, OnSettingChanged);
	
	cvShopEventPriceThunderStorm = CreateConVar("hg_shop_event_thunderstorm", "0", "Price for buying an thunder storm event (0: Disable).");
	g_iShopEventPriceThunderStorm = GetConVarInt(cvShopEventPriceThunderStorm);
	HookConVarChange(cvShopEventPriceThunderStorm, OnSettingChanged);
	
	cvShopEventPriceChargedWater = CreateConVar("hg_shop_event_chargedwater", "4000", "Price for buying an charged water event (0: Disable).");
	g_iShopEventPriceChargedWater = GetConVarInt(cvShopEventPriceChargedWater);
	HookConVarChange(cvShopEventPriceChargedWater, OnSettingChanged);
	
	cvShopEventPriceEarthquake = CreateConVar("hg_shop_event_earthquake", "0", "Price for buying an earthquake event (0: Disable).");
	g_iShopEventPriceEarthquake = GetConVarInt(cvShopEventPriceEarthquake);
	HookConVarChange(cvShopEventPriceEarthquake, OnSettingChanged);
	
	cvShopEventPriceArtillery = CreateConVar("hg_shop_event_artillery", "10000", "Price for buying an artillery event (0: Disable).");
	g_iShopEventPriceArtillery = GetConVarInt(cvShopEventPriceArtillery);
	HookConVarChange(cvShopEventPriceArtillery, OnSettingChanged);
	
	cvShopBinocularsPrice = CreateConVar("hg_shop_binoculars", "25", "Price for buying a binoculars (0: Disable).");
	g_iShopBinocularsPrice = GetConVarInt(cvShopBinocularsPrice);
	HookConVarChange(cvShopBinocularsPrice, OnSettingChanged);
	
	cvShopCompassPrice = CreateConVar("hg_shop_compass", "35", "Price for buying a compass (0: Disable).");
	g_iShopCompassPrice = GetConVarInt(cvShopCompassPrice);
	HookConVarChange(cvShopCompassPrice, OnSettingChanged);
	
	cvShopTracerPrice = CreateConVar("hg_shop_tracer", "350", "Price for buying a tracer (0: Disable).");
	g_iShopTracerPrice = GetConVarInt(cvShopTracerPrice);
	HookConVarChange(cvShopTracerPrice, OnSettingChanged);
	
	cvShopJammerPrice = CreateConVar("hg_shop_jammer", "700", "Price for buying a jammer (0: Disable).");
	g_iShopJammerPrice = GetConVarInt(cvShopJammerPrice);
	HookConVarChange(cvShopJammerPrice, OnSettingChanged);
	
	cvShopGpsPrice = CreateConVar("hg_shop_gps", "500", "Price for buying a GPS (0: Disable).");
	g_iShopGpsPrice = GetConVarInt(cvShopGpsPrice);
	HookConVarChange(cvShopGpsPrice, OnSettingChanged);
	
	cvShopParachutePrice = CreateConVar("hg_shop_parachute", "50", "Price for buying a parachute (0: Disable).");
	g_iShopParachutePrice = GetConVarInt(cvShopParachutePrice);
	HookConVarChange(cvShopParachutePrice, OnSettingChanged);
	
	/* Survival */
	
	cvSurvival = CreateConVar("hg_survival", "1", "Enable survival system.");
	g_bSurvival = GetConVarBool(cvSurvival);
	HookConVarChange(cvSurvival, OnSettingChanged);
	
	g_cvSurvivalCheckInterval = CreateConVar("hg_survival_check_interval", "1.0", "Delay between player status checks.");
	g_fSurvivalCheckInterval = GetConVarFloat(g_cvSurvivalCheckInterval);
	HookConVarChange(g_cvSurvivalCheckInterval, OnSettingChanged);
	
	/* Check Speed */
	
	g_cvSpeedWalking = CreateConVar("hg_survival_check_speed_walking", "35.0", "When player is moving faster than this he is walking.");
	g_fSpeedWalking = GetConVarFloat(g_cvSpeedWalking);
	HookConVarChange(g_cvSpeedWalking, OnSettingChanged);
	
	g_cvSpeedRunning = CreateConVar("hg_survival_check_speed_running", "160.0", "When player is moving faster than this he is running.");
	g_fSpeedRunning = GetConVarFloat(g_cvSpeedRunning);
	HookConVarChange(g_cvSpeedRunning, OnSettingChanged);

	/* Hunger */
	
	g_cvEnergyDefault = CreateConVar("hg_survival_energy_default", "100.0", "Default % for energy when player spawns.");
	g_fEnergyDefault = GetConVarFloat(g_cvEnergyDefault);
	HookConVarChange(g_cvEnergyDefault, OnSettingChanged);
	
	g_cvHungerDefault = CreateConVar("hg_survival_hunger_default", "100.0", "Default % for hunger when player spawns.");
	g_fHungerDefault = GetConVarFloat(g_cvHungerDefault);
	HookConVarChange(g_cvHungerDefault, OnSettingChanged);
	
	g_cvStarveCamping = CreateConVar("hg_survival_hunger_camping", "0.05", "Starving speed while camping (recudes energy).");
	g_fStarveCamping = GetConVarFloat(g_cvStarveCamping);
	HookConVarChange(g_cvStarveCamping, OnSettingChanged);
	
	g_cvStarveWalking = CreateConVar("hg_survival_hunger_walking", "0.1", "Starving speed while walking (recudes energy).");
	g_fStarveWalking = GetConVarFloat(g_cvStarveWalking);
	HookConVarChange(g_cvStarveWalking, OnSettingChanged);
	
	g_cvStarveRunning = CreateConVar("hg_survival_hunger_running", "0.2", "Starving speed while running (recudes energy).");
	g_fStarveRunning = GetConVarFloat(g_cvStarveRunning);
	HookConVarChange(g_cvStarveRunning, OnSettingChanged);
	
	g_cvHungerCamping = CreateConVar("hg_survival_energy_consumption_camping", "0.1", "How fast players become hungry while camping.");
	g_fHungerCamping = GetConVarFloat(g_cvHungerCamping);
	HookConVarChange(g_cvHungerCamping, OnSettingChanged);
	
	g_cvHungerWalking = CreateConVar("hg_survival_energy_consumption_walking", "0.15", "How fast players become hungry while walking.");
	g_fHungerWalking = GetConVarFloat(g_cvHungerWalking);
	HookConVarChange(g_cvHungerWalking, OnSettingChanged);
	
	g_cvHungerRunning = CreateConVar("hg_survival_energy_consumption_running", "0.25", "How fast players become hungry while running.");
	g_fHungerRunning = GetConVarFloat(g_cvHungerRunning);
	HookConVarChange(g_cvHungerRunning, OnSettingChanged);
	
	/* Health */
	
	g_cvHealthStarvingPassage = CreateConVar("hg_survival_health_starving_passage", "50.0", "When energy+hunger falls below this value players can loose health (-1: disable).");
	g_fHealthStarvingPassage = GetConVarFloat(g_cvHealthStarvingPassage);
	HookConVarChange(g_cvHealthStarvingPassage, OnSettingChanged);
	
	g_cvHealthRecoveryPassage = CreateConVar("hg_survival_health_recovery_passage", "100.0", "When energy+hunger stays above this value players can recover health (-1: disable).");
	g_fHealthRecoveryPassage = GetConVarFloat(g_cvHealthRecoveryPassage);
	HookConVarChange(g_cvHealthRecoveryPassage, OnSettingChanged);
	
	g_cvHealthRecoveryCost = CreateConVar("hg_survival_health_recovery_cost", "0.5", "Additionally energy comsumption rate when recovering health (0.0: disable).");
	g_fHealthRecoveryCost = GetConVarFloat(g_cvHealthRecoveryCost);
	HookConVarChange(g_cvHealthRecoveryCost, OnSettingChanged);
	
	g_cvHealthMin = CreateConVar("hg_survival_health_min", "0", "Min health by starving (0: Players can die by starving)");
	g_iHealthMin = GetConVarInt(g_cvHealthMin);
	HookConVarChange(g_cvHealthMin, OnSettingChanged);
	
	g_cvHealthMax = CreateConVar("hg_survival_health_max", "100", "Max health to regen when eating food.");
	g_iHealthMax = GetConVarInt(g_cvHealthMax);
	HookConVarChange(g_cvHealthMax, OnSettingChanged);
	
	g_cvHealthGain = CreateConVar("hg_survival_health_recovery", "0.34", "Health a player can gain each check.");
	g_fHealthGain = GetConVarFloat(g_cvHealthGain);
	HookConVarChange(g_cvHealthGain, OnSettingChanged);
	
	g_cvHealthLose = CreateConVar("hg_survival_health_starving", "1.3", "Health a player can lose each check.");
	g_fHealthLose = GetConVarFloat(g_cvHealthLose);
	HookConVarChange(g_cvHealthLose, OnSettingChanged);

	/* Thirst */
	
	g_cvThirstDefault = CreateConVar("hg_survival_thirst_default", "100.0", "Default % for thirst.");
	g_fThirstDefault = GetConVarFloat(g_cvThirstDefault);
	HookConVarChange(g_cvThirstDefault, OnSettingChanged);
	
	g_cvThirstCamping = CreateConVar("hg_survival_thirst_camping", "0.04", "Dehydration rate while camping.");
	g_fThirstCamping = GetConVarFloat(g_cvThirstCamping);
	HookConVarChange(g_cvThirstCamping, OnSettingChanged);
	
	g_cvThirstWalking = CreateConVar("hg_survival_thirst_walking", "0.13", "Dehydration rate while walking.");
	g_fThirstWalking = GetConVarFloat(g_cvThirstWalking);
	HookConVarChange(g_cvThirstWalking, OnSettingChanged);
	
	g_cvThirstRunning = CreateConVar("hg_survival_thirst_running", "1.1", "Dehydration rate while running.");
	g_fThirstRunning = GetConVarFloat(g_cvThirstRunning);
	HookConVarChange(g_cvThirstRunning, OnSettingChanged);
	
	g_cvThirstWaterDrinkRate = CreateConVar("hg_survival_thirst_drink_rate", "20.0", "How much % thirst to restore each sec. when player is in water.");
	g_fThirstWaterDrinkRate = GetConVarFloat(g_cvThirstWaterDrinkRate);
	HookConVarChange(g_cvThirstWaterDrinkRate, OnSettingChanged);

	/* Stamina */
	
	g_cvStaminaDefault = CreateConVar("hg_survival_stamina_default", "100.0", "Default % for stamina.");
	g_fStaminaDefault = GetConVarFloat(g_cvStaminaDefault);
	HookConVarChange(g_cvStaminaDefault, OnSettingChanged);
	
	g_cvStaminaRunningCost = CreateConVar("hg_survival_stamina_running_cost", "2.2", "How fast players loosing stamina while running.");
	g_fStaminaRunningCost = GetConVarFloat(g_cvStaminaRunningCost);
	HookConVarChange(g_cvStaminaRunningCost, OnSettingChanged);
	
	g_cvStaminaJumpCost = CreateConVar("hg_survival_stamina_jump_cost", "8.5", "How much stamina to pay for jumping.");
	g_fStaminaJumpCost = GetConVarFloat(g_cvStaminaJumpCost);
	HookConVarChange(g_cvStaminaJumpCost, OnSettingChanged);
	
	g_cvStaminaRecoverBuffer = CreateConVar("hg_survival_stamina_recover_buffer", "1.2", "Max recovery rate taken from stamina buffer.");
	g_fStaminaRecoverBuffer = GetConVarFloat(g_cvStaminaRecoverBuffer);
	HookConVarChange(g_cvStaminaRecoverBuffer, OnSettingChanged);
	
	g_cvStaminaRecoverBase = CreateConVar("hg_survival_stamina_recover_base", "2.3", "Base recovery rate for stamina while walking.");
	g_fStaminaRecoverBase = GetConVarFloat(g_cvStaminaRecoverBase);
	HookConVarChange(g_cvStaminaRecoverBase, OnSettingChanged);
	
	g_cvStaminaRecoverBasePerc = CreateConVar("hg_survival_stamina_recover_base_perc", "0.15", "Recover additionally % of players reserve stamina.");
	g_fStaminaRecoverBasePerc = GetConVarFloat(g_cvStaminaRecoverBasePerc);
	HookConVarChange(g_cvStaminaRecoverBasePerc, OnSettingChanged);
	
	g_cvStaminaRecoverThirstPerc = CreateConVar("hg_survival_stamina_recover_thirst_perc", "0.06", "Recover additionally % of players thirst level.");
	g_fStaminaRecoverThirstPerc = GetConVarFloat(g_cvStaminaRecoverThirstPerc);
	HookConVarChange(g_cvStaminaRecoverThirstPerc, OnSettingChanged);
	
	g_cvStaminaRecoverVIP = CreateConVar("hg_survival_stamina_recover_vip", "1.0", "Recovery multi for VIP 1.5 makes it 50% faster. (Does not include hg_survival_stamina_recover_base_perc)");
	g_fStaminaRecoverVIP = GetConVarFloat(g_cvStaminaRecoverVIP);
	HookConVarChange(g_cvStaminaRecoverVIP, OnSettingChanged);
	
	/* Speed */
	
	g_cvStaminaPerfectSpeed = CreateConVar("hg_survival_speed_perfect", "1.1", "Running speed with full stamina.");
	g_fStaminaPerfectSpeed = GetConVarFloat(g_cvStaminaPerfectSpeed);
	HookConVarChange(g_cvStaminaPerfectSpeed, OnSettingChanged);
	
	g_cvStaminaGoodSpeed = CreateConVar("hg_survival_speed_good", "1.0", "Running speed with almost full stamina.");
	g_fStaminaGoodSpeed = GetConVarFloat(g_cvStaminaGoodSpeed);
	HookConVarChange(g_cvStaminaGoodSpeed, OnSettingChanged);
	
	g_cvStaminaLowSpeed = CreateConVar("hg_survival_speed_low", "0.95", "Running speed with almost low stamina.");
	g_fStaminaLowSpeed = GetConVarFloat(g_cvStaminaLowSpeed);
	HookConVarChange(g_cvStaminaLowSpeed, OnSettingChanged);
	
	g_cvStaminaVeryLowSpeed = CreateConVar("hg_survival_speed_verylow", "0.9", "Running speed with very low stamina.");
	g_fStaminaVerySpeed = GetConVarFloat(g_cvStaminaVeryLowSpeed);
	HookConVarChange(g_cvStaminaVeryLowSpeed, OnSettingChanged);
	
	g_cvStaminaEmptySpeed = CreateConVar("hg_survival_speed_empty", "0.8", "Running speed with empty stamina.");
	g_fStaminaEmptySpeed = GetConVarFloat(g_cvStaminaEmptySpeed);
	HookConVarChange(g_cvStaminaEmptySpeed, OnSettingChanged);

	/* Knockout */
	
	cvKnockoutFake = CreateConVar("hg_knockout_fake", "1", "Allow tributes to fake their death once per round.");
	g_bKnockoutFake = GetConVarBool(cvKnockoutFake);
	HookConVarChange(cvKnockoutFake, OnSettingChanged);
	
	g_cvKnockoutChance = CreateConVar("hg_knockout_chance", "0.2", "Chance for getting knocked out when receiving a crit hit which was not lethal (0.2 = 20%).");
	g_fKnockoutChance = GetConVarFloat(g_cvKnockoutChance);
	HookConVarChange(g_cvKnockoutChance, OnSettingChanged);
	
	g_cvKnockoutDamage = CreateConVar("hg_knockout_damage", "65", "Min damage which can cause the player being knocked out.");
	g_iKnockoutDamage = GetConVarInt(g_cvKnockoutDamage);
	HookConVarChange(g_cvKnockoutDamage, OnSettingChanged);
	
	g_cvKnockoutTimeMin = CreateConVar("hg_knockout_time_min", "5.0", "Min time a player gets knocked out.");
	g_fKnockoutTimeMin = GetConVarFloat(g_cvKnockoutTimeMin);
	HookConVarChange(g_cvKnockoutTimeMin, OnSettingChanged);
	
	g_cvKnockoutTimeMax = CreateConVar("hg_knockout_time_max", "10.0", "Max time a player gets knocked out.");
	g_fKnockoutTimeMax = GetConVarFloat(g_cvKnockoutTimeMax);
	HookConVarChange(g_cvKnockoutTimeMax, OnSettingChanged);
	
	cvFakedeathCmds = CreateConVar("hg_knockout_fake_cmds", "!fake;!fakedeath;!opossum;fake;!fk", "Fakedeath commands seperated by \";\" (max 8 commands).");
	GetConVarString(cvFakedeathCmds, g_sFakedeathCmds, sizeof(g_sFakedeathCmds));
	HookConVarChange(cvFakedeathCmds, OnSettingChanged);
	GetFakedeathCommands();
	
	cvSuicideCmds = CreateConVar("hg_suicide_cmds", "!suicide;!sc;suicide;sc", "Suicide commands seperated by \";\" (max 8 commands).");
	GetConVarString(cvSuicideCmds, g_sSuicideCmds, sizeof(g_sSuicideCmds));
	HookConVarChange(cvSuicideCmds, OnSettingChanged);
	GetSuicideCommands();
	
	/* Stats */
	
	cvStatsCmds = CreateConVar("hg_stats_cmds", "!stats;stats;!statsme;statsme;!rank;rank;!rankme;rankme", "Stats(me) commands seperated by \";\" (max 8 commands).");
	GetConVarString(cvStatsCmds, g_sStatsCmds, sizeof(g_sStatsCmds));
	HookConVarChange(cvStatsCmds, OnSettingChanged);
	GetStatsCommands();
	
	cvTopCmds = CreateConVar("hg_top_cmds", "!top;top;!top10;top10", "Top stats menu commands seperated by \";\" (max 8 commands).");
	GetConVarString(cvTopCmds, g_sTopCmds, sizeof(g_sTopCmds));
	HookConVarChange(cvTopCmds, OnSettingChanged);
	GetTopCommands();
	
	cvStatsMeShowAll = CreateConVar("hg_stats_statsme_showall", "1", "Show statsme in chat for all other players too or not.");
	g_bStatsMeShowAll = view_as<bool>(GetConVarInt(cvStatsMeShowAll));
	HookConVarChange(cvStatsMeShowAll, OnSettingChanged);
	
	cvStatsMeMessages = CreateConVar("hg_stats_statsme_messages", "3", "How much chat lines should be used for the statsme command [Max 3].");
	g_iStatsMeMessages = GetConVarInt(cvStatsMeMessages);
	HookConVarChange(cvStatsMeMessages, OnSettingChanged);
	
	cvTopLimit = CreateConVar("hg_stats_top_limit", "10", "Limit top menu to x players.");
	g_iTopLimit = GetConVarInt(cvTopLimit);
	HookConVarChange(cvTopLimit, OnSettingChanged);
	
	/* Sounds */
	
	cvSndWelcome = CreateConVar("hg_snd_welcome", "hg/welcome.mp3", "Welcome sound");
	GetConVarString(cvSndWelcome, sndWelcome, sizeof(sndWelcome));
	HookConVarChange(cvSndWelcome, OnSettingChanged);
	g_bSndWelcome = strlen(sndWelcome) > 6;
	
	cvSndRoundstart = CreateConVar("hg_snd_roundstart", "hg/roundstart.mp3", "Roundstart sound");
	GetConVarString(cvSndRoundstart, sndRoundStart, sizeof(sndRoundStart));
	HookConVarChange(cvSndRoundstart, OnSettingChanged);
	g_bSndRoundstart = strlen(sndRoundStart) > 6;
	
	cvSndRoundstart2 = CreateConVar("hg_snd_roundstart2", "hg/roundstart2.mp3", "Roundstart alternative sound");
	GetConVarString(cvSndRoundstart2, sndRoundStart2, sizeof(sndRoundStart2));
	HookConVarChange(cvSndRoundstart2, OnSettingChanged);
	g_bSndRoundstart2 = strlen(sndRoundStart2) > 6;
	
	cvSndRoundDraw = CreateConVar("hg_snd_rounddraw", "hg/rounddraw.mp3", "Rounddraw sound");
	GetConVarString(cvSndRoundDraw, sndRoundDraw, sizeof(sndRoundDraw));
	HookConVarChange(cvSndRoundDraw, OnSettingChanged);
	g_bSndRoundDraw = strlen(sndRoundDraw) > 6;
	
	cvSndCannon = CreateConVar("hg_snd_cannon", "hg/cannon.mp3", "Cannon sound (player died)");
	GetConVarString(cvSndCannon, sndCannon, sizeof(sndCannon));
	HookConVarChange(cvSndCannon, OnSettingChanged);
	g_bSndCannon = strlen(sndCannon) > 6;
	
	cvSndEndgame = CreateConVar("hg_snd_endgame", "hg/mockingjay.mp3", "Endgame sound");
	GetConVarString(cvSndEndgame, sndEndgame, sizeof(sndEndgame));
	HookConVarChange(cvSndEndgame, OnSettingChanged);
	g_bSndEndgame = strlen(sndEndgame) > 6;
	
	cvSndBeacon = CreateConVar("hg_snd_beacon", "hg/beacon.mp3", "Beacon sound");
	GetConVarString(cvSndBeacon, sndBeacon, sizeof(sndBeacon));
	HookConVarChange(cvSndBeacon, OnSettingChanged);
	g_bSndBeacon = strlen(sndBeacon) > 6;
	
	cvSndKnifeFight = CreateConVar("hg_snd_knifefight", "hg/knifefight.mp3", "Knifefight sound");
	GetConVarString(cvSndKnifeFight, sndKnifeFight, sizeof(sndKnifeFight));
	HookConVarChange(cvSndKnifeFight, OnSettingChanged);
	g_bSndKnifeFight = strlen(sndKnifeFight) > 6;
	
	cvSndWave = CreateConVar("hg_snd_wave", "hg/horde.mp3", "Wave spawned sound");
	GetConVarString(cvSndWave, sndWave, sizeof(sndWave));
	HookConVarChange(cvSndWave, OnSettingChanged);
	g_bSndWave = strlen(sndWave) > 6;
	
	cvSndFireworks1 = CreateConVar("hg_snd_fireworks1", "hg/firework/firework_1.mp3", "Fireworks sound nr. 1");
	GetConVarString(cvSndFireworks1, sndFireworks1, sizeof(sndFireworks1));
	HookConVarChange(cvSndFireworks1, OnSettingChanged);
	g_bSndFireworks1 = strlen(sndFireworks1) > 6;
	
	cvSndFireworks2 = CreateConVar("hg_snd_fireworks2", "hg/firework/firework_2.mp3", "Fireworks sound nr. 2");
	GetConVarString(cvSndFireworks2, sndFireworks2, sizeof(sndFireworks2));
	HookConVarChange(cvSndFireworks2, OnSettingChanged);
	g_bSndFireworks2 = strlen(sndFireworks2) > 6;
	
	cvSndFireworks3 = CreateConVar("hg_snd_fireworks3", "hg/firework/firework_3.mp3", "Fireworks sound nr. 3");
	GetConVarString(cvSndFireworks3, sndFireworks3, sizeof(sndFireworks3));
	HookConVarChange(cvSndFireworks3, OnSettingChanged);
	g_bSndFireworks3 = strlen(sndFireworks3) > 6;
	
	cvSndFireworks4 = CreateConVar("hg_snd_fireworks4", "hg/firework/firework_4.mp3", "Fireworks sound nr. 4");
	GetConVarString(cvSndFireworks4, sndFireworks4, sizeof(sndFireworks4));
	HookConVarChange(cvSndFireworks4, OnSettingChanged);
	g_bSndFireworks4 = strlen(sndFireworks4) > 6;
	
	cvSndIonApproaching = CreateConVar("hg_snd_ion_approaching", "hg/ion/approaching.mp3", "Ion cannon approaching sound");
	GetConVarString(cvSndIonApproaching, sndIonApproaching, sizeof(sndIonApproaching));
	HookConVarChange(cvSndIonApproaching, OnSettingChanged);
	g_bSndIonApproaching = strlen(sndIonApproaching) > 6;
	
	cvSndIonCharging = CreateConVar("hg_snd_ion_charging", "hg/ion/charging.mp3", "Ion cannon charging sound");
	GetConVarString(cvSndIonCharging, sndIonCharging, sizeof(sndIonCharging));
	HookConVarChange(cvSndIonCharging, OnSettingChanged);
	g_bSndIonCharging = strlen(sndIonCharging) > 6;
	
	cvSndIonDeployed = CreateConVar("hg_snd_ion_deployed", "hg/ion/deployed.mp3", "Ion cannon deployed sound");
	GetConVarString(cvSndIonDeployed, sndIonDeployed, sizeof(sndIonDeployed));
	HookConVarChange(cvSndIonDeployed, OnSettingChanged);
	g_bSndIonDeployed = strlen(sndIonDeployed) > 6;
	
	cvSndIonImplosion = CreateConVar("hg_snd_ion_implosion", "hg/ion/implosion.mp3", "Ion cannon implosion sound");
	GetConVarString(cvSndIonImplosion, sndIonImplosion, sizeof(sndIonImplosion));
	HookConVarChange(cvSndIonImplosion, OnSettingChanged);
	g_bSndIonImplosion = strlen(sndIonImplosion) > 6;
	
	cvSndEarthquake = CreateConVar("hg_snd_earthquake", "hg/earthquake.mp3", "Earthquake sound");
	GetConVarString(cvSndEarthquake, sndEarthquake, sizeof(sndEarthquake));
	HookConVarChange(cvSndEarthquake, OnSettingChanged);
	g_bSndEarthquake = strlen(sndEarthquake) > 6;

	/* VIP */
	
	cvVIPflags = CreateConVar("hg_vip_flag", "a", "Flags used for vip features.");
	char sFlag[32]; GetConVarString(cvVIPflags, sFlag, sizeof(sFlag));
	g_iVIPflags = ReadFlagString(sFlag);
	HookConVarChange(cvTopLimit, OnSettingChanged);
	
	/* Loot */
	
	g_cvLoopWipeInterval = CreateConVar("hg_loot_wipe_interval", "0.0", "Interval starting from roundstart to respawn all loot (0.0: Disabled).");
	g_fLoopWipeInterval = GetConVarFloat(g_cvLoopWipeInterval);
	HookConVarChange(g_cvLoopWipeInterval, OnSettingChanged);
	
	RegisterEvents();
	
	AutoExecConfig(true, "hungergames");
	
	/* Other */
	
	cvImmunityAlpha = FindConVar("sv_disable_immunity_alpha");
	SetConVarBool(cvImmunityAlpha, true);
	HookConVarChange(cvImmunityAlpha, OnSettingChanged);
}

public int OnSettingChanged(Handle convar, const char[] oldValue, const char[] newValue)
{
	if(convar == cvPrefix)
		strcopy(g_sPrefix, sizeof(g_sPrefix), newValue);
	else if (convar == g_cvEndgameMaxPlayers)
		g_iEndgameMaxPlayers = StringToInt(newValue);
	else if (convar == g_cvEndgameMinDelay)
		g_fEndgameMinDelay = StringToFloat(newValue);
	else if (convar == g_cvRoundEndDelay)
		g_fRoundEndDelay = StringToFloat(newValue);
	else if (convar == g_cvCheckInterval)
	{
		g_fCheckInterval = StringToFloat(newValue);
		StartPlayerCheckTimer();
	}
	else if (convar == g_cvDeathDelay)
		g_fDeathDelay = StringToFloat(newValue);
	else if (convar == cvDeathTeamChange)
		g_bDeathTeamChange = view_as<bool>(StringToInt(newValue));
	
	/* Give Items Delayed */
	
	else if(convar == cvGiveKnifeDelay)
		g_fGiveKnifeDelay = StringToFloat(newValue);
	else if(convar == cvGiveBinocularsDelay)
		g_fGiveBinocularsDelay = StringToFloat(newValue);
	else if(convar == cvGiveCompassDelay)
		g_fGiveCompassDelay = StringToFloat(newValue);
	else if(convar == cvGiveParachuteDelay)
		g_fGiveParachuteDelay = StringToFloat(newValue);
		
	/* HUD */
	
	else if(convar == cvHudTickInterval)
		g_fHudTickInterval = StringToFloat(newValue);
	else if(convar == cvHudMaxTicks)
		g_iHudMaxTicks = StringToInt(newValue);
	else if(convar == cvHudEquipmentTicks)
		g_iHudEquipmentTicks = StringToInt(newValue);
	else if(convar == cvHudCompassTicks)
		g_iHudCompassTicks = StringToInt(newValue);
		
	/* Endgame */
	
	else if(convar == cvBeaconInterval)
		g_fBeaconInterval = StringToFloat(newValue);
	else if(convar == cvKnifeFightDelay)
		g_fKnifeFightDelay = StringToFloat(newValue);
	else if(convar == cvEndgameWipeLoot)
		g_bEndgameWipeLoot = view_as<bool>(StringToInt(newValue));
		
	/* Weapon Cleaner */
	
	else if(convar == cvWeaponCleanerInterval)
		g_fWeaponCleanerInterval = StringToFloat(newValue);
	else if(convar == cvWeaponCleanerDistance)
		g_fWeaponCleanerDistance = StringToFloat(newValue);
	
	/* Throwing Knifes */
	
	else if(convar == cvThrowingKnifesDamage)
		g_iThrowingKnifesDamage = StringToInt(newValue);
	else if(convar == cvThrowingKnifesDamageHeadshot)
		g_iThrowingKnifesDamageHeadshot = StringToInt(newValue);

	/* Fireworks */
	
	else if(convar == cvFireworksEnabled)
		g_bFireworksEnabled = view_as<bool>(StringToInt(newValue));

	/* Zombies */
	
	else if(convar == cvZombieActivateDelay)
		g_fZombieActivateDelay = StringToFloat(newValue);
	else if(convar == cvZombieWaveInterval)
		g_fZombieWaveInterval = StringToFloat(newValue);
	else if(convar == cvZombieSpeedDefault)
		g_fZombieSpeedDefault = StringToFloat(newValue);
	else if(convar == cvZombieSpeedNear)
		g_fZombieSpeedNear = StringToFloat(newValue);
	else if(convar == cvZombieSpeedDistance)
		g_fZombieSpeedDistance = StringToFloat(newValue);
	else if(convar == cvZombieGravity)
		g_fZombieGravity = StringToFloat(newValue);
	else if(convar == cvZombieHealth)
		g_iZombieHealth = StringToInt(newValue);
	else if(convar == cvZombieArmor)
		g_iZombieArmor = StringToInt(newValue);
	else if(convar == cvZombieHeal)
		g_iZombieHeal = StringToInt(newValue);
	else if(convar == cvZombieFOV)
		g_iZombieFOV = StringToInt(newValue);
	else if(convar == cvZombieEndgame)
		g_bZombieEndgame = view_as<bool>(StringToInt(newValue));
	else if(convar == cvZombieThirdperson)
		g_bZombieThirdperson = view_as<bool>(StringToInt(newValue));
	else if(convar == cvZombieModel)
	{
		strcopy(g_sZombieModel, sizeof(g_sZombieModel), newValue);
		
		if(!IsModelPrecached(g_sZombieModel))
			PrecacheModel(g_sZombieModel);
	}
	else if(convar == cvZombieArmsModel)
	{
		strcopy(g_sZombieArmsModel, sizeof(g_sZombieArmsModel), newValue);
		
		if(!IsModelPrecached(g_sZombieArmsModel))
			PrecacheModel(g_sZombieArmsModel);
	}
	
	/* Parachute */
	
	else if(convar == cvParachuteModel)
	{
		strcopy(g_sParachuteModel, sizeof(g_sParachuteModel), newValue);
		
		if(!IsModelPrecached(g_sParachuteModel))
			PrecacheModel(g_sParachuteModel);
	}
	else if(convar == cvParachuteFallspeedMax)
		fParachuteFallspeedMax = StringToFloat(newValue);
	else if(convar == cvParachuteAllowShooting)
		bParachuteAllowShooting = view_as<bool>(StringToInt(newValue));
	
	/* Voice */
	
	else if(convar == cvVoiceTributeRange)
		g_fVoiceTributeRange = StringToFloat(newValue);
	else if(convar == cvVoiceImmunity)
		g_iVoiceImmunity = StringToInt(newValue);
		
	/* Events */
	
	else if(convar == cvEventDelay)
		g_fEventDelay = StringToFloat(newValue);
	else if(convar == cvEventEndgame)
		g_bEventEndgame = view_as<bool>(StringToInt(newValue));
	else if(convar == cvEventIntervalMin)
		g_fEventIntervalMin = StringToFloat(newValue);
	else if(convar == cvEventIntervalMax)
		g_fEventIntervalMax = StringToFloat(newValue);
	else if(convar == cvEventVoteTime)
		g_fEventVoteTime = StringToFloat(newValue);
	else if(convar == cvEventIonCannonEnable)
	{
		g_iEventIonCannon = StringToInt(newValue);
		RegisterEvents();
	}
	else if(convar == cvEventIonCannonStartRange)
		g_fEventIonCannonStartRange = StringToFloat(newValue);
	else if(convar == cvEventIonCannonUpdateInterval)
		g_fEventIonCannonUpdateInterval = StringToFloat(newValue);
	else if(convar == cvEventNapalmEnable)
	{
		g_iEventNapalm = StringToInt(newValue);
		RegisterEvents();
	}
	else if(convar == cvEventNapalmProjectiles)
		g_iEventNapalmProjectiles = StringToInt(newValue);
	else if(convar == cvEventNapalmRadius)
		g_fEventNapalmRadius = StringToFloat(newValue);
	else if(convar == cvEventThunderStormEnable)
	{
		g_iEventThunderStorm = StringToInt(newValue);
		RegisterEvents();
	}
	else if(convar == cvEventThunderStormDamage)
		g_iEventThunderStormDamage = StringToInt(newValue);
	else if(convar == cvEventThunderStormLightnings)
		g_iEventThunderStormLightnings = StringToInt(newValue);
	else if(convar == cvEventThunderStormInterval)
		g_fEventThunderStormInterval = StringToFloat(newValue);
	else if(convar == cvEventChargedWaterEnable)
	{
		g_iEventChargedWater = StringToInt(newValue);
		RegisterEvents();
	}
	else if(convar == cvEventChargedWaterDamage)
		g_iEventChargedWaterDamage = StringToInt(newValue);
	else if(convar == cvEventChargedWaterSimulationsMax)
		g_iEventChargedWaterSimulationsMax = StringToInt(newValue);
	else if(convar == cvEventChargedWaterInterval)
		g_fEventChargedWaterInterval = StringToFloat(newValue);
	else if(convar == cvEventEarthquakeEnable)
	{
		g_iEventEarthquake = StringToInt(newValue);
		RegisterEvents();
	}
	else if(convar == cvEventEarthquakeIntensity)
		g_fEventEarthquakeIntensity = StringToFloat(newValue);
	else if(convar == cvEventEarthquakeFrequency)
		g_fEventEarthquakeFrequency = StringToFloat(newValue);
	else if(convar == cvEventEarthquakeDuration)
		g_fEventEarthquakeDuration = StringToFloat(newValue);
	else if(convar == cvEventArtilerieEnable)
		g_iEventArtilerie = StringToInt(newValue);
	else if(convar == cvEventArtilerieMagnitude)
		g_iEventArtilerieMagnitude = StringToInt(newValue);
	else if(convar == cvEventArtillerieRadius)
		g_fEventArtillerieRadius = StringToFloat(newValue);
	else if(convar == cvEventArtillerieProjectiles)
		g_iEventArtillerieProjectiles = StringToInt(newValue);
	
	/* Proxy Mine */
	
	else if(convar == cvMineMagnitude)
		g_iMineMagnitude = StringToInt(newValue);
	else if(convar == cvMineAbortExplode)
		g_bMineAbortExplode = view_as<bool>(StringToInt(newValue));
	
	/* Party */
	
	else if(convar == cvPartyLimit)
		g_iPartyLimit = StringToInt(newValue);
	else if(convar == cvPartyMaxDistance)
		g_fPartyMaxDistance = StringToFloat(newValue);
	else if(convar == cvPartyDmgMateAttacksMate)
		g_fPartyDmgMateAttacksMate = StringToFloat(newValue);
	else if(convar == cvPartyDmgPartyAttacksSingle)
		g_fPartyDmgPartyAttacksSingle = StringToFloat(newValue);
	else if(convar == cvPartyDmgPartyAttacksParty)
		g_fPartyDmgPartyAttacksParty = StringToFloat(newValue);
	else if(convar == cvPartyDmgSingleAttacksParty)
		g_fPartyDmgSingleAttacksParty = StringToFloat(newValue);
	else if(convar == cvPartyResetRoundstartEnabled)
		g_bPartyResetRoundstart = view_as<bool>(StringToInt(newValue));
	else if(convar == cvPartyResetEndgame)
		g_iPartyResetEndgame = StringToInt(newValue);
	else if(convar == cvPartyVIP)
		g_bPartyVIP = view_as<bool>(StringToInt(newValue));
		
	/* Compass */
	
	else if(convar == cvCompassDisorientationDistance)
		g_fCompassDisorientationDistance = StringToFloat(newValue);
	else if(convar == cvCompassShowNameDistance)
		g_fCompassShowNameDistance = StringToFloat(newValue);
	else if(convar == cvCompassAllowParty)
		g_bCompassAllowParty = view_as<bool>(StringToInt(newValue));

	/* TA Grenade */
	
	else if(convar == cvTagrenadeVisible)
		g_bTagrenadeVisible = view_as<bool>(StringToInt(newValue));
	else if(convar == cvTagrenadeRange)
		g_fTagrenadeRange = StringToFloat(newValue);
	else if(convar == cvTagrenadeTime)
		g_fTagrenadeTime = StringToFloat(newValue);

	/* Points */
	
	else if(convar == cvPointsKill)
		g_iPointsKill = StringToInt(newValue);
	else if(convar == cvPointsKnifeKill)
		g_iPointsKnifeKill = StringToInt(newValue);
	else if(convar == cvPointsWin)
		g_iPartyLimit = StringToInt(newValue);
	else if(convar == cvPointsWin)
		g_iPartyLimit = StringToInt(newValue);
	else if(convar == cvPointsZombieKill)
		g_iPointsZombieKill = StringToInt(newValue);
	else if(convar == cvPointsKillAsZombie)
		g_iPointsKillAsZombie = StringToInt(newValue);
	else if(convar == cvPointsPlaytime)
		g_iPointsPlaytime = StringToInt(newValue);
	else if(convar == cvPointsDefuseMine)
		g_iPointsDefuseMine = StringToInt(newValue);
	else if(convar == cvPointsEndgame)
		g_iPointsEndgame = StringToInt(newValue);
	else if(convar == cvPointsFinalKnifefight)
		g_iPointsFinalKnifefight = StringToInt(newValue);
	else if(convar == cvPointsVIP)
		g_fPointsVIP = StringToFloat(newValue);

	/* Shop */
	
	else if(convar == cvShopSelfOnly)
		g_bShopSelfOnly = view_as<bool>(StringToInt(newValue));
	else if(convar == cvShopAlive)
		g_bShopAlive = view_as<bool>(StringToInt(newValue));
	else if(convar == cvShopDead)
		g_bShopDead = view_as<bool>(StringToInt(newValue));
	else if(convar == cvShopCmds)
	{
		strcopy(g_sShopCmds, sizeof(g_sShopCmds), newValue);
		GetShopCommands();
	}
	else if(convar == cvShopEventTime)
		g_fShopEventTime = StringToFloat(newValue);
	else if(convar == cvShopSpawnTime)
		g_fShopSpawnTime = StringToFloat(newValue);
	else if(convar == cvShopEvents)
		g_bShopEvents = view_as<bool>(StringToInt(newValue));
	else if(convar == cvShopEventPriceIonCannon)
		g_iShopEventPriceIonCannon = StringToInt(newValue);
	else if(convar == cvShopEventPriceNapalm)
		g_iShopEventPriceNapalm = StringToInt(newValue);
	else if(convar == cvShopEventPriceThunderStorm)
		g_iShopEventPriceThunderStorm = StringToInt(newValue);
	else if(convar == cvShopEventPriceChargedWater)
		g_iShopEventPriceChargedWater = StringToInt(newValue);
	else if(convar == cvShopEventPriceEarthquake)
		g_iShopEventPriceEarthquake = StringToInt(newValue);
	else if(convar == cvShopEventPriceArtillery)
		g_iShopEventPriceArtillery = StringToInt(newValue);
		
		
	else if(convar == cvShopBinocularsPrice)
		g_iShopBinocularsPrice = StringToInt(newValue);
	else if(convar == cvShopCompassPrice)
		g_iShopCompassPrice = StringToInt(newValue);
	else if(convar == cvShopTracerPrice)
		g_iShopTracerPrice = StringToInt(newValue);
	else if(convar == cvShopJammerPrice)
		g_iShopJammerPrice = StringToInt(newValue);
	else if(convar == cvShopGpsPrice)
		g_iShopGpsPrice = StringToInt(newValue);
	else if(convar == cvShopParachutePrice)
		g_iShopParachutePrice = StringToInt(newValue);
	
	/* Survival */
	
	else if(convar == cvSurvival)
		g_bSurvival = view_as<bool>(StringToInt(newValue));
	else if (convar == g_cvSurvivalCheckInterval)
	{
		g_fSurvivalCheckInterval = StringToFloat(newValue);
		StartSurvivalTimer();
	}
	else if (convar == g_cvSpeedWalking)
		g_fSpeedWalking = StringToFloat(newValue);
	else if (convar == g_cvSpeedRunning)
		g_fSpeedRunning = StringToFloat(newValue);
	else if (convar == g_cvHungerDefault)
		g_fHungerDefault = StringToFloat(newValue);
	else if (convar == g_cvEnergyDefault)
		g_fEnergyDefault = StringToFloat(newValue);
	else if (convar == g_cvStarveCamping)
		g_fStarveCamping = StringToFloat(newValue);
	else if (convar == g_cvStarveWalking)
		g_fStarveWalking = StringToFloat(newValue);
	else if (convar == g_cvStarveRunning)
		g_fStarveRunning = StringToFloat(newValue);
	else if (convar == g_cvHungerCamping)
		g_fHungerCamping = StringToFloat(newValue);
	else if (convar == g_cvHungerWalking)
		g_fHungerWalking = StringToFloat(newValue);
	else if (convar == g_cvHungerRunning)
		g_fHungerRunning = StringToFloat(newValue);
	else if (convar == g_cvHealthStarvingPassage)
		g_fHealthStarvingPassage = StringToFloat(newValue);
	else if (convar == g_cvHealthRecoveryPassage)
		g_fHealthRecoveryPassage = StringToFloat(newValue);
	else if (convar == g_cvHealthRecoveryCost)
		g_fHealthRecoveryCost = StringToFloat(newValue);
	else if (convar == g_cvHealthMin)
		g_iHealthMin = StringToInt(newValue);
	else if (convar == g_cvHealthMax)
		g_iHealthMax = StringToInt(newValue);
	else if (convar == g_cvHealthGain)
		g_fHealthGain = StringToFloat(newValue);
	else if (convar == g_cvHealthLose)
		g_fHealthLose = StringToFloat(newValue);
	else if (convar == g_cvThirstDefault)
		g_fThirstDefault = StringToFloat(newValue);
	else if (convar == g_cvThirstCamping)
		g_fThirstCamping = StringToFloat(newValue);
	else if (convar == g_cvThirstWalking)
		g_fThirstWalking = StringToFloat(newValue);
	else if (convar == g_cvThirstRunning)
		g_fThirstRunning = StringToFloat(newValue);
	else if (convar == g_cvThirstWaterDrinkRate)
		g_fThirstWaterDrinkRate = StringToFloat(newValue);
	else if (convar == g_cvStaminaDefault)
		g_fStaminaDefault = StringToFloat(newValue);
	else if (convar == g_cvStaminaRunningCost)
		g_fStaminaRunningCost = StringToFloat(newValue);
	else if (convar == g_cvStaminaJumpCost)
		g_fStaminaJumpCost = StringToFloat(newValue);
	else if (convar == g_cvStaminaRecoverBuffer)
		g_fStaminaRecoverBuffer = StringToFloat(newValue);
	else if (convar == g_cvStaminaRecoverBase)
		g_fStaminaRecoverBase = StringToFloat(newValue);
	else if (convar == g_cvStaminaRecoverBasePerc)
		g_fStaminaRecoverBasePerc = StringToFloat(newValue);
	else if (convar == g_cvStaminaRecoverThirstPerc)
		g_fStaminaRecoverThirstPerc = StringToFloat(newValue);
	else if (convar == g_cvStaminaRecoverVIP)
		g_fStaminaRecoverVIP = StringToFloat(newValue);
	else if (convar == g_cvStaminaPerfectSpeed)
		g_fStaminaPerfectSpeed = StringToFloat(newValue);
	else if (convar == g_cvStaminaGoodSpeed)
		g_fStaminaGoodSpeed = StringToFloat(newValue);
	else if (convar == g_cvStaminaLowSpeed)
		g_fStaminaLowSpeed = StringToFloat(newValue);
	else if (convar == g_cvStaminaVeryLowSpeed)
		g_fStaminaVerySpeed = StringToFloat(newValue);
	else if (convar == g_cvStaminaEmptySpeed)
		g_fStaminaEmptySpeed = StringToFloat(newValue);
	
	/* Knockout */
	
	else if(convar == cvKnockoutFake)
		g_bKnockoutFake = view_as<bool>(StringToInt(newValue));
	else if (convar == g_cvKnockoutChance)
		g_fKnockoutChance = StringToFloat(newValue);
	else if (convar == g_cvKnockoutDamage)
		g_iKnockoutDamage = StringToInt(newValue);
	else if (convar == g_cvKnockoutTimeMin)
		g_fKnockoutTimeMin = StringToFloat(newValue);
	else if (convar == g_cvKnockoutTimeMax)
		g_fKnockoutTimeMax = StringToFloat(newValue);	
	else if (convar == cvFakedeathCmds)
	{
		strcopy(g_sFakedeathCmds, sizeof(g_sFakedeathCmds), newValue);
		GetFakedeathCommands();
	}
	else if (convar == cvSuicideCmds)
	{
		strcopy(g_sSuicideCmds, sizeof(g_sSuicideCmds), newValue);
		GetSuicideCommands();
	}
	
	/* Stats */
	
	else if (convar == cvStatsCmds)
	{
		strcopy(g_sStatsCmds, sizeof(g_sStatsCmds), newValue);
		GetStatsCommands();
	}
	else if (convar == cvTopCmds)
	{
		strcopy(g_sTopCmds, sizeof(g_sTopCmds), newValue);
		GetTopCommands();
	}
	else if(convar == cvStatsMeShowAll)
		g_bStatsMeShowAll = view_as<bool>(StringToInt(newValue));
	else if(convar == cvStatsMeMessages)
		g_iStatsMeMessages = StringToInt(newValue);
	else if(convar == cvTopLimit)
		g_iTopLimit = StringToInt(newValue);
	
	/* Sounds */
	
	else if(convar == cvSndWelcome)
	{
		strcopy(sndWelcome, sizeof(sndWelcome), newValue);
		g_bSndWelcome = strlen(sndWelcome) > 6;
		if(g_bSndWelcome)
			PrecacheSoundAnyDownload(sndWelcome);
	}
	else if(convar == cvSndRoundstart)
	{
		strcopy(sndRoundStart, sizeof(sndRoundStart), newValue);
		g_bSndRoundstart = strlen(sndRoundStart) > 6;
		if(g_bSndRoundstart)
			PrecacheSoundAnyDownload(sndRoundStart);
	}
	else if(convar == cvSndRoundstart2)
	{
		strcopy(sndRoundStart2, sizeof(sndRoundStart2), newValue);
		g_bSndRoundstart2 = strlen(sndRoundStart2) > 6;
		if(g_bSndRoundstart2)
			PrecacheSoundAnyDownload(sndRoundStart2);
	}
	else if(convar == cvSndRoundDraw)
	{
		strcopy(sndRoundDraw, sizeof(sndRoundDraw), newValue);
		g_bSndRoundDraw = strlen(sndRoundDraw) > 6;
		if(g_bSndRoundDraw)
			PrecacheSoundAnyDownload(sndRoundDraw);
	}
	else if(convar == cvSndCannon)
	{
		strcopy(sndCannon, sizeof(sndCannon), newValue);
		g_bSndCannon = strlen(sndCannon) > 6;
		if(g_bSndCannon)
			PrecacheSoundAnyDownload(sndCannon);
	}
	else if(convar == cvSndEndgame)
	{
		strcopy(sndEndgame, sizeof(sndEndgame), newValue);
		g_bSndEndgame = strlen(sndEndgame) > 6;
		if(g_bSndEndgame)
			PrecacheSoundAnyDownload(sndEndgame);
	}
	else if(convar == cvSndBeacon)
	{
		strcopy(sndBeacon, sizeof(sndBeacon), newValue);
		g_bSndBeacon = strlen(sndBeacon) > 6;
		if(g_bSndBeacon)
			PrecacheSoundAnyDownload(sndBeacon);
	}
	else if(convar == cvSndKnifeFight)
	{
		strcopy(sndKnifeFight, sizeof(sndKnifeFight), newValue);
		g_bSndKnifeFight = strlen(sndKnifeFight) > 6;
		if(g_bSndKnifeFight)
			PrecacheSoundAnyDownload(sndKnifeFight);
	}
	else if(convar == cvSndWave)
	{
		strcopy(sndWave, sizeof(sndWave), newValue);
		g_bSndWave = strlen(sndWave) > 6;
		if(g_bSndWave)
			PrecacheSoundAnyDownload(sndWave);
	}
	else if(convar == cvSndFireworks1)
	{
		strcopy(sndFireworks1, sizeof(sndFireworks1), newValue);
		g_bSndFireworks1 = strlen(sndFireworks1) > 6;
		if(g_bSndFireworks1)
			PrecacheSoundAnyDownload(sndFireworks1);
	}
	else if(convar == cvSndFireworks2)
	{
		strcopy(sndFireworks2, sizeof(sndFireworks2), newValue);
		g_bSndFireworks2 = strlen(sndFireworks2) > 6;
		if(g_bSndFireworks2)
			PrecacheSoundAnyDownload(sndFireworks2);
	}
	else if(convar == cvSndFireworks3)
	{
		strcopy(sndFireworks3, sizeof(sndFireworks3), newValue);
		g_bSndFireworks3 = strlen(sndFireworks3) > 6;
		if(g_bSndFireworks3)
			PrecacheSoundAnyDownload(sndFireworks3);
	}
	else if(convar == cvSndFireworks4)
	{
		strcopy(sndFireworks4, sizeof(sndFireworks4), newValue);
		g_bSndFireworks4 = strlen(sndFireworks4) > 6;
		if(g_bSndFireworks4)
			PrecacheSoundAnyDownload(sndFireworks4);
	}
	else if(convar == cvSndIonApproaching)
	{
		strcopy(sndIonApproaching, sizeof(sndIonApproaching), newValue);
		g_bSndIonApproaching = strlen(sndIonApproaching) > 6;
		if(g_bSndIonApproaching)
			PrecacheSoundAnyDownload(sndIonApproaching);
	}
	else if(convar == cvSndIonCharging)
	{
		strcopy(sndIonCharging, sizeof(sndIonCharging), newValue);
		g_bSndIonCharging = strlen(sndIonCharging) > 6;
		if(g_bSndIonCharging)
			PrecacheSoundAnyDownload(sndIonCharging);
	}
	else if(convar == cvSndIonDeployed)
	{
		strcopy(sndIonDeployed, sizeof(sndIonDeployed), newValue);
		g_bSndIonDeployed = strlen(sndIonDeployed) > 6;
		if(g_bSndIonDeployed)
			PrecacheSoundAnyDownload(sndIonDeployed);
	}
	else if(convar == cvSndIonImplosion)
	{
		strcopy(sndIonImplosion, sizeof(sndIonImplosion), newValue);
		g_bSndIonImplosion = strlen(sndIonImplosion) > 6;
		if(g_bSndIonImplosion)
			PrecacheSoundAnyDownload(sndIonImplosion);
	}
	else if(convar == cvSndEarthquake)
	{
		strcopy(sndEarthquake, sizeof(sndEarthquake), newValue);
		g_bSndEarthquake = strlen(sndEarthquake) > 6;
		if(g_bSndEarthquake)
			PrecacheSoundAnyDownload(sndEarthquake);
	}

	/* VIP */
	
	else if(convar == cvVIPflags)
		g_iVIPflags = ReadFlagString(newValue);
	

	/* Loot */
	
	else if (convar == g_cvLoopWipeInterval)
		g_fLoopWipeInterval = StringToFloat(newValue);
	
	/* Other */
	
	else if (convar == cvImmunityAlpha)
	{
		if(!view_as<bool>(StringToInt(newValue)))
			SetConVarBool(cvImmunityAlpha, true);
	}
}

void PrepareSounds()
{
	PrecacheSoundAnyDownload(sndRoundStart);
	PrecacheSoundAnyDownload(sndRoundStart2);
	PrecacheSoundAnyDownload(sndRoundDraw);
	PrecacheSoundAnyDownload(sndKnifeFight);
	PrecacheSoundAnyDownload(sndCannon);
	PrecacheSoundAnyDownload(sndBeacon);
	PrecacheSoundAnyDownload(sndEndgame);
	PrecacheSoundAnyDownload(sndWelcome);
	PrecacheSoundAnyDownload(sndWave);
	
	for (int i = 0; i < sizeof(sndCountdown); i++)
		PrecacheSoundAnyDownload(sndCountdown[i]);
	
	for(new i = 0; i < sizeof(sndThunderStorm); i++)
		PrecacheSound(sndThunderStorm[i]);
	
	if(g_bFireworksEnabled)
	{
		PrecacheSoundAnyDownload(sndFireworks1);
		PrecacheSoundAnyDownload(sndFireworks2);
		PrecacheSoundAnyDownload(sndFireworks3);
		PrecacheSoundAnyDownload(sndFireworks4);
	}
	
	if(g_iEventIonCannon > 0)
	{
		PrecacheSoundAnyDownload(sndIonApproaching);
		PrecacheSoundAnyDownload(sndIonCharging);
		PrecacheSoundAnyDownload(sndIonDeployed);
		PrecacheSoundAnyDownload(sndIonImplosion);
	}
	
	if(g_iEventEarthquake > 0)
		PrecacheSoundAnyDownload(sndEarthquake);
}

#define VMT_BOMBRING "materials/sprites/bomb_planted_ring.vmt"
#define VMT_LASERBEAM "materials/sprites/laserbeam.vmt"
#define VMT_HALO "materials/sprites/halo.vmt"
#define VMT_FIREBALL "materials/effects/fire_cloud1.vmt"
#define VMT_BLUEGLOW "materials/sprites/blueglow1.vmt"

void PrepareMaterialsAndModels()
{
	g_iBombRing = PrecacheModel(VMT_BOMBRING);
	g_iLaser = PrecacheModel(VMT_LASERBEAM);
	g_iHalo = PrecacheModel(VMT_HALO);
	g_iFireSmoke = PrecacheModel(VMT_FIREBALL);
	g_iBlueGlow = PrecacheModel(VMT_BLUEGLOW);
	
	if(!IsModelPrecached(g_sZombieModel))
		PrecacheModel(g_sZombieModel);
	
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_1_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_1_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_2_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_2_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_3_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_3_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_4_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_4_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_5_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_5_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_6_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_6_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_7_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_7_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_8_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_8_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_9_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_9_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_10_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_10_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_11_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_11_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_12_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_12_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_13_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_13_128.vmt");
		
	if(!IsModelPrecached("materials/sprites/hg/districts/drunken/district_c_128.vmt"))
		PrecacheModel("materials/sprites/hg/districts/drunken/district_c_128.vmt");
		
	PrecacheModel("models/weapons/w_eq_molotov_dropped.mdl");
}

void PrecacheSoundAnyDownload(char[] sSound)
{
	PrecacheSoundAny(sSound);
	
	char sBuffer[256];
	Format(sBuffer, sizeof(sBuffer), "sound/%s", sSound);
	AddFileToDownloadsTable(sBuffer);
}

void GetShopCommands()
{
	if(g_hTrie_ShopCmds == null)
		g_hTrie_ShopCmds = CreateTrie();
	else ClearTrie(g_hTrie_ShopCmds);
	
	char sBuffer[256];
	char sDatas[16][256];
	
	strcopy(sBuffer, sizeof(sBuffer), g_sShopCmds);
	
	TrimString(sBuffer);
	ExplodeString(sBuffer, ";", sDatas, sizeof(sDatas), sizeof(sBuffer));
	
	int iCmds = ReplaceString(sBuffer, sizeof(sBuffer), ";", "")+1;
	
	for (int i = 0; i < iCmds; i++)
	{
		if(strlen(sDatas[i]) < 1)
			break;
		
		SetTrieValue(g_hTrie_ShopCmds, sDatas[i], 1);
	}
}

void GetFakedeathCommands()
{
	if(g_hTrie_FakedeathCmds == null)
		g_hTrie_FakedeathCmds = CreateTrie();
	else ClearTrie(g_hTrie_FakedeathCmds);
	
	char sBuffer[256];
	char sDatas[16][256];
	
	strcopy(sBuffer, sizeof(sBuffer), g_sFakedeathCmds);
	
	TrimString(sBuffer);
	ExplodeString(sBuffer, ";", sDatas, sizeof(sDatas), sizeof(sBuffer));
	
	int iCmds = ReplaceString(sBuffer, sizeof(sBuffer), ";", "")+1;
	
	for (int i = 0; i < iCmds; i++)
	{
		if(strlen(sDatas[i]) < 1)
			break;
		
		SetTrieValue(g_hTrie_FakedeathCmds, sDatas[i], 1);
	}
}

void GetSuicideCommands()
{
	if(g_hTrie_SuicideCmds == null)
		g_hTrie_SuicideCmds = CreateTrie();
	else ClearTrie(g_hTrie_SuicideCmds);
	
	char sBuffer[256];
	char sDatas[16][256];
	
	strcopy(sBuffer, sizeof(sBuffer), g_sSuicideCmds);
	
	TrimString(sBuffer);
	ExplodeString(sBuffer, ";", sDatas, sizeof(sDatas), sizeof(sBuffer));
	
	int iCmds = ReplaceString(sBuffer, sizeof(sBuffer), ";", "")+1;
	
	for (int i = 0; i < iCmds; i++)
	{
		if(strlen(sDatas[i]) < 1)
			break;
		
		SetTrieValue(g_hTrie_SuicideCmds, sDatas[i], 1);
	}
}

void GetStatsCommands()
{
	if(g_hTrie_StatsCmds == null)
		g_hTrie_StatsCmds = CreateTrie();
	else ClearTrie(g_hTrie_StatsCmds);
	
	char sBuffer[256];
	char sDatas[16][256];
	
	strcopy(sBuffer, sizeof(sBuffer), g_sStatsCmds);
	
	TrimString(sBuffer);
	ExplodeString(sBuffer, ";", sDatas, sizeof(sDatas), sizeof(sBuffer));
	
	int iCmds = ReplaceString(sBuffer, sizeof(sBuffer), ";", "")+1;
	
	for (int i = 0; i < iCmds; i++)
	{
		if(strlen(sDatas[i]) < 1)
			break;
		
		SetTrieValue(g_hTrie_StatsCmds, sDatas[i], 1);
	}
}

void GetTopCommands()
{
	if(g_hTrie_TopCmds == null)
		g_hTrie_TopCmds = CreateTrie();
	else ClearTrie(g_hTrie_TopCmds);
	
	char sBuffer[256];
	char sDatas[16][256];
	
	strcopy(sBuffer, sizeof(sBuffer), g_sTopCmds);
	
	TrimString(sBuffer);
	ExplodeString(sBuffer, ";", sDatas, sizeof(sDatas), sizeof(sBuffer));
	
	int iCmds = ReplaceString(sBuffer, sizeof(sBuffer), ";", "")+1;
	
	for (int i = 0; i < iCmds; i++)
	{
		if(strlen(sDatas[i]) < 1)
			break;
		
		SetTrieValue(g_hTrie_TopCmds, sDatas[i], 1);
	}
}