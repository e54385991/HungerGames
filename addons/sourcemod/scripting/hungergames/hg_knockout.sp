int g_iRagdollRef[MAXPLAYERS+1] = {INVALID_ENT_REFERENCE, ...};
int g_iCameraRef[MAXPLAYERS+1] = {INVALID_ENT_REFERENCE, ...};

Handle g_hKnockoutTimer[MAXPLAYERS+1] = {null, ...};

bool g_bFakeDeathUsed[MAXPLAYERS + 1];

void Cmd_FakeDeath(int iClient)
{
	if(g_bKnockoutFake && iClient && IsClientInGame(iClient) && IsPlayerAlive(iClient) && !IsKnockedOut(iClient))
	{
		if(!g_bIsTribute[iClient])
		{
			CPrintToChat(iClient, "%T", "KnockoutNotTribute", iClient, g_sPrefix);
			return;
		}
		
		if(IsFinal())
		{
			CPrintToChat(iClient, "%T", "KnockoutFinal", iClient, g_sPrefix);
			return;
		}
		
		/*
		if(g_bFakeDeathUsed[iClient])
		{
			CPrintToChat(iClient, "%T", "KnockoutLimited", iClient, g_sPrefix);
			return;
		}*/
		
		g_bFakeDeathUsed[iClient] = true;
		KnockoutPlayer(iClient);
	}
}

void Cmd_Suicide(int iClient)
{
	if(iClient > 0 && IsPlayerAlive(iClient))
		ForcePlayerSuicide(iClient);
}

void ResetFakeDeath(int iClient)
{
	g_bFakeDeathUsed[iClient] = false;
}

void ResetKnockout(int iClient, bool mapchange)
{
	int iEntity = EntRefToEntIndex(g_iRagdollRef[iClient]);
	if(iEntity != INVALID_ENT_REFERENCE)
		AcceptEntityInput(iEntity, "kill");
	
	g_iRagdollRef[iClient] = INVALID_ENT_REFERENCE;
	
	int iEntity2 = EntRefToEntIndex(g_iCameraRef[iClient]);
	if(iEntity2 != INVALID_ENT_REFERENCE)
		AcceptEntityInput(iEntity2, "kill");
	
	g_iCameraRef[iClient] = INVALID_ENT_REFERENCE;
	
	if(!mapchange && g_hKnockoutTimer[iClient] != null)
		CloseHandle(g_hKnockoutTimer[iClient]);
	
	g_hKnockoutTimer[iClient] = null;
	
	if(!IsClientInGame(iClient))
		return;
	
	SetEntityRenderMode(iClient, RENDER_TRANSCOLOR);
	Entity_SetMoveable(iClient);
	SetClientViewEntity(iClient, iClient);
	
	if(GetParty(iClient) != -1)
		CreateIcon(iClient);
}

bool IsKnockedOut(int iClient)
{
	return EntRefToEntIndex(g_iRagdollRef[iClient]) != INVALID_ENT_REFERENCE;
}

void KnockoutPlayer(int iClient)
{
	if(IsZombie(iClient))
		return;
		
	if(IsKnockedOut(iClient))
		return;
		
	if(IsFinal())
		return;
	
	UnHookSkin(iClient);
	CPS_RemoveSkin(iClient);
	
	char sModel[256];
	GetClientModel(iClient, sModel, sizeof(sModel));

	float pos[3];
	GetClientEyePosition(iClient, pos);

	int iEntity = CreateEntityByName("prop_ragdoll");
	DispatchKeyValue(iEntity, "model", sModel);
	SetEntProp(iEntity, Prop_Data, "m_nSolidType", 6);
	SetEntProp(iEntity, Prop_Data, "m_CollisionGroup", 5);
	DispatchSpawn(iEntity);
	
	Entity_SetCollisionGroup(iEntity, COLLISION_GROUP_DEBRIS_TRIGGER);

	pos[2] -= 16.0;
	TeleportEntity(iEntity, pos, NULL_VECTOR, NULL_VECTOR);

	g_iRagdollRef[iClient] = EntIndexToEntRef(iEntity);
	SetEntityRenderMode(iClient, RENDER_NONE);
	StripPlayerWeapons(iClient, false, true);
	Entity_SetNonMoveable(iClient);
	
	float fDuration = GetRandomFloat(g_fKnockoutTimeMin, g_fKnockoutTimeMax);
	g_hKnockoutTimer[iClient] = CreateTimer(fDuration, Timer_EndKnockout, iClient, TIMER_FLAG_NO_MAPCHANGE);

	SpawnCamAndAttach(iClient, iEntity);
	
	SetEntProp(iEntity, Prop_Data, "m_takedamage", DAMAGE_EVENTS_ONLY, 1);
	SDKHook(iEntity, SDKHook_OnTakeDamage, OnDamageRagdoll);
	
	ResetPartyIcon(iClient);
}

public Action Timer_EndKnockout(Handle timer, any iClient)
{
	g_hKnockoutTimer[iClient] = null;
	
	ResetKnockout(iClient, false);
}

stock bool SpawnCamAndAttach(int iClient, int iRagdoll)
{
	char sModel[64];
	Format(sModel, sizeof(sModel), "models/blackout.mdl"); //
	PrecacheModel(sModel, true);

	char sTargetName[64]; 
	Format(sTargetName, sizeof(sTargetName), "ragdoll%d", iClient);
	DispatchKeyValue(iRagdoll, "targetname", sTargetName);

	int iEntity = CreateEntityByName("prop_dynamic");
	if (iEntity == -1)
		return false;

	char sCamName[64]; 
	Format(sCamName, sizeof(sCamName), "ragdollCam%d", iEntity);

	DispatchKeyValue(iEntity, "targetname", sCamName);
	DispatchKeyValue(iEntity, "parentname", sTargetName);
	DispatchKeyValue(iEntity, "model",	  sModel);
	DispatchKeyValue(iEntity, "solid",	  "0");
	DispatchKeyValue(iEntity, "rendermode", "10"); // dont render
	DispatchKeyValue(iEntity, "disableshadows", "1"); // no shadows

	float fAngles[3]; 
	GetClientEyeAngles(iClient, fAngles);
	
	char sCamAngles[64];
	Format(sCamAngles, 64, "%f %f %f", fAngles[0], fAngles[1], fAngles[2]);
	
	DispatchKeyValue(iEntity, "angles", sCamAngles);

	SetEntityModel(iEntity, sModel);
	DispatchSpawn(iEntity);

	SetVariantString(sTargetName);
	AcceptEntityInput(iEntity, "SetParent", iEntity, iEntity, 0);

	SetVariantString("facemask");
	AcceptEntityInput(iEntity, "SetParentAttachment", iEntity, iEntity, 0);

	AcceptEntityInput(iEntity, "TurnOn");

	SetClientViewEntity(iClient, iEntity);
	g_iCameraRef[iClient] = EntIndexToEntRef(iEntity);

	return true;
}

public Action OnDamageRagdoll(int ragdoll, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3])
{
	int iTarget = -1;
	
	LoopIngameClients(i)
	{
		if(EntRefToEntIndex(g_iRagdollRef[i]) != ragdoll)
			continue;
		
		iTarget = i;
	}
	
	if(iTarget == -1)
		return Plugin_Continue;
	
	int iAttacker = attacker;
	
	if(iAttacker <= 0)
		iAttacker = inflictor;
	
	if(iAttacker <= 0)
		return Plugin_Continue;
	
	char sWeapon[64];
	if(weapon <= 0)
		strcopy(sWeapon, sizeof(sWeapon), "knife_default_ct");
	else GetEdictClassname(weapon, sWeapon, sizeof(sWeapon));
	
	Point_Hurt(iTarget, RoundToFloor(damage) + 10, iAttacker, damagetype, sWeapon);
	
	return Plugin_Continue;
}
