// Credits: SWAT_88

void DownloadFiles()
{
	char file[256];
	BuildPath(Path_SM, file, 255, "configs/hg_files.txt");
	Handle fileh = OpenFile(file, "r");
	
	char sBuffer[256];
	int len;
	
	if(fileh == null) 
		return;
		
	while (ReadFileLine(fileh, sBuffer, sizeof(sBuffer)))
	{
		len = strlen(sBuffer);
		if (sBuffer[len-1] == '\n')
			sBuffer[--len] = '\0';

		TrimString(sBuffer);

		if(!StrEqual(sBuffer,"",false))
			ReadFileFolder(sBuffer);
		
		if (IsEndOfFile(fileh))
			break;
	}
	
	if(fileh != null)
		CloseHandle(fileh);
}

void ReadFileFolder(char[] path)
{
	Handle dirh = null;
	char sBuffer[256];
	char tmp_path[256];
	FileType type = FileType_Unknown;
	int len;
	
	len = strlen(path);
	if (path[len-1] == '\n')
		path[--len] = '\0';

	TrimString(path);
	
	if(DirExists(path))
	{
		dirh = OpenDirectory(path);
		while(ReadDirEntry(dirh,sBuffer,sizeof(sBuffer),type))
		{
			len = strlen(sBuffer);
			if (sBuffer[len-1] == '\n')
				sBuffer[--len] = '\0';

			TrimString(sBuffer);

			if (!StrEqual(sBuffer,"",false) && !StrEqual(sBuffer,".",false) && !StrEqual(sBuffer,"..",false))
			{
				strcopy(tmp_path,255,path);
				StrCat(tmp_path,255,"/");
				StrCat(tmp_path,255,sBuffer);
				if(type == FileType_File)
					ReadItem(tmp_path);
				else ReadFileFolder(tmp_path);
			}
		}
	}
	else ReadItem(path);
	
	if(dirh != null)
		CloseHandle(dirh);
}

void ReadItem(char[] sBuffer)
{
	int len = strlen(sBuffer);
	if (sBuffer[len-1] == '\n')
		sBuffer[--len] = '\0';
	
	TrimString(sBuffer);
	if(len >= 2 && sBuffer[0] == '/' && sBuffer[1] == '/')
	{
		//Comment
	}
	else if (!StrEqual(sBuffer,"",false) && FileExists(sBuffer))
		AddFileToDownloadsTable(sBuffer);
}