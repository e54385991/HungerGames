void Stats_AddPlayerDeath(int client)
{
	if(!g_bConnected || !g_pZcoreMysql || !client || !IsClientInGame(client) || IsFakeClient(client) || !g_bLoadedSQL[client])
		return;
	
	int iTime = GetTime();
	char sQuery[2048];
	FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET deaths = deaths+1, lastplay = %d, playtime = playtime+%d WHERE playerID = %s", iTime, g_iPlaytimeTemp[client], g_sAuth[client]);
	SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
	
	g_iDeaths[client]++;
	g_iPlaytimeTemp[client] = 0;	
}

void Stats_AddKill(int client, bool knife)
{
	int iTime = GetTime();
	
	/* Update knife kill count */
	if(knife)
	{
		if(!IsFakeClient(client) && g_bLoadedSQL[client])
		{
			char sQuery[2048];
			FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET kills = kills+1, knifekills = knifekills+1, lastplay = %d, playtime = playtime+%d WHERE playerID = %s", iTime, g_iPlaytimeTemp[client], g_sAuth[client]);
			SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
		}
		
		g_iKnifeKills[client]++;
	}
	
	/* Update kill count */
	else
	{
		if(!IsFakeClient(client) && g_bLoadedSQL[client])
		{
			char sQuery[2048];
			FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET kills = kills+1, lastplay = %d, playtime = playtime+%d WHERE playerID = %s", iTime, g_iPlaytimeTemp[client], g_sAuth[client]);
			SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
		}
		
	}
	
	g_iKills[client]++;
	g_iPlaytimeTemp[client] = 0;
}

void UpdateElo(int client)
{
	int iTime = GetTime();
	
	char sQuery[2048];
	FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET elo = %d, eloMax = %d, lastplay = %d, points = points+%d, playtime = playtime+%d WHERE playerID = %s", g_iElo[client], g_iEloMax[client], iTime, g_iPointsTemp[client], g_iPlaytimeTemp[client], g_sAuth[client]);
	SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
}

bool Stats_AddPoints(int client, int iPoints, bool forceUpdate)
{
	if(!g_bConnected || !g_pZcoreMysql || !client || !IsClientInGame(client) || IsFakeClient(client) || !g_bLoadedSQL[client])
		return false;
	
	int iTime = GetTime();
	
	g_iPoints[client] += iPoints;
	
	if(forceUpdate)
	{
		char sQuery[512];
		FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET lastplay = %d, points = points+%d WHERE playerID = %s", iTime, iPoints+g_iPointsTemp[client], g_sAuth[client]);
		SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
		g_iPointsTemp[client] = 0;
	}
	else g_iPointsTemp[client] += iPoints;
	
	return true;
}

bool Stats_RemovePoints(int client, int iPoints)
{
	if(!g_bConnected || !g_pZcoreMysql || !client || !IsClientInGame(client) || IsFakeClient(client) || !g_bLoadedSQL[client])
		return false;
	
	if (g_iPoints[client] < iPoints)
		return false;
	
	int iTime = GetTime();
	
	g_iPoints[client] -= iPoints;
	
	if(g_iPointsTemp[client] >= iPoints)
	{
		g_iPointsTemp[client] -= iPoints;
	}
	else
	{
		char sQuery[512];
		FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET lastplay = %d, points = points-%d WHERE playerID = %s", iTime, iPoints, g_sAuth[client]);
		SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
	}
	
	return true;
}

void Stats_UpdatePlaytime(int client)
{
	if(!g_bConnected || !g_pZcoreMysql || !client || !IsClientInGame(client) || IsFakeClient(client) || !g_bLoadedSQL[client])
		return;
		
	int iTime = GetTime();
	
	/* Update playtime */
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET lastplay = %d, playtime = playtime+%d WHERE playerID = %s", iTime, g_iPlaytimeTemp[client], g_sAuth[client]);
	SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
	
	g_iPlaytimeTemp[client] = 0;
}