void Stats_ELO(int attacker, int victim, bool knife)
{
	if(!g_bConnected || !g_pZcoreMysql || !attacker || !IsClientInGame(attacker) || !victim || !IsClientInGame(victim))
		return;
	
	if(IsFakeClient(attacker))
		g_iElo[attacker] = 1000;
		
	if(IsFakeClient(victim))
		g_iElo[victim] = 1000;
	
	/* Update ELO stats */
	if(g_iElo[attacker] > 0 && g_iElo[victim] > 0)
	{
		int iEloDiff = CalcElo(attacker, victim);
		if(iEloDiff)
		{
			g_iElo[attacker] += iEloDiff;
			g_iElo[victim] -= iEloDiff;
			
			if(g_iElo[victim] < 1)
				g_iElo[victim] = 1; //poor guy
			
			if(g_iElo[attacker] > g_iEloMax[attacker])
				g_iEloMax[attacker] = g_iElo[attacker];
			
			if(g_iElo[victim] < g_iEloMin[victim])
				g_iEloMin[victim] = g_iElo[victim];
		}
		
		if(!IsFakeClient(attacker) && g_bLoadedSQL[attacker])
		{
			int iPoints = (knife ? g_iPointsKnifeKill : g_iPointsKill);
			
			if(Client_HasAdminFlags(attacker, g_iVIPflags))
				iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
			
			char sVictimName[32];
			GetClientName(victim, sVictimName, sizeof(sVictimName));
			
			char sMsg[512];
			Format(sMsg, sizeof(sMsg), "%T", "EloGained", attacker, g_sPrefix, g_iElo[attacker], iEloDiff, sVictimName);
			
			if(g_fDeathDelay == 0.0)
				CPrintToChat(attacker, sMsg);
			else DelayDeadRelatedMessage(attacker, sMsg);
			
			g_iPoints[attacker] += iPoints;
			g_iPointsTemp[attacker] = 0;
			
			UpdateElo(attacker);
		}
		
		if(!IsFakeClient(victim) && g_bLoadedSQL[victim])
		{
			char sAttackerName[32];
			GetClientName(attacker, sAttackerName, sizeof(sAttackerName));
			
			CPrintToChat(victim, "%T", "EloLost", victim, g_sPrefix, g_iElo[victim], iEloDiff, sAttackerName);
			
			UpdateElo(victim);
		}
	}
}

int CalcElo(int attacker, int victim)
{
	float prob = 1/(Pow(10.0, float((g_iElo[victim]-g_iElo[attacker]))/400)+1);
	return RoundToFloor(25.0*(1-prob));
}