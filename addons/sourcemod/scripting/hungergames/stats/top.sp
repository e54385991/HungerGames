void Cmd_Top(int client)
{
	Menu_Top(client);
}

void Menu_Top(int client)
{
	Menu menu = new Menu(Menu_Main_Callback);
	menu.SetTitle("Top Stats Menu");
	
	char sBuffer[128];
	
	Format(sBuffer, sizeof(sBuffer), "%T", "MenuTitle_Top_Wins", client);
	menu.AddItem("wins", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "MenuTitle_Top_Kills", client);
	menu.AddItem("kills", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "MenuTitle_Top_KnifeKills", client);
	menu.AddItem("knifekills", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "MenuTitle_Top_ELO", client);
	menu.AddItem("elo", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "MenuTitle_Top_Playtime", client);
	menu.AddItem("playtime", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "MenuTitle_Top_Points", client);
	menu.AddItem("points", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "MenuTitle_Top_KDR", client);
	menu.AddItem("kdr", sBuffer);
	
	menu.ExitButton = true;
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int Menu_Main_Callback(Menu menu, MenuAction action, int client, int info)
{
	if (action == MenuAction_Select)
	{
		char sInfo[32];
		menu.GetItem(info, sInfo, sizeof(sInfo));
		
		LoadTopPlayers(client, sInfo);
	}
	else if(action == MenuAction_End)
		delete menu;
}

void LoadTopPlayers(int client, char[] field, bool ASC = false)
{
	if(!g_bConnected || !g_pZcoreMysql)
	{
		PrintToChat(client, "%T", "NoDatabaseConnection", client, g_sPrefix);
		return;
	}
	
	char sQuery[512];
	
	if(StrEqual(field, "kdr"))
		FormatEx(sQuery, sizeof(sQuery), "SELECT `playerID`,`name`,`kills`/`deaths` FROM `hg_players` WHERE `kills` >= 50 ORDER BY `kills`/`deaths` %s LIMIT %d", ASC ? "ASC" : "DESC", g_iTopLimit);
	else FormatEx(sQuery, sizeof(sQuery), "SELECT `playerID`,`name`,`%s` FROM `hg_players` WHERE 1 ORDER BY `%s` %s LIMIT %d", field, field, ASC ? "ASC" : "DESC", g_iTopLimit);
	
	DataPack pack = new DataPack();
	SQL_TQuery(g_hSQL, CallBack_Top, sQuery, pack);
	
	pack.WriteCell(GetClientUserId(client));
	pack.WriteString(field);
}

public void CallBack_Top(Handle owner, Handle hndl, const char[] error, any pack)
{
	if (owner == null || hndl == null)
	{
		CloseHandle(pack);
		DatabaseError("CallBack_Top", error);
		return;
	}
	
	/* Pack */
	
	ResetPack(pack);
	
	int client = GetClientOfUserId(ReadPackCell(pack));
	
	char sFieldName[32];
	ReadPackString(pack, sFieldName, sizeof(sFieldName));
	
	CloseHandle(pack);
	
	/* Is client still valid */
	
	if(client <= 0)
		return;
	
	/* Build top menu */
	
	Menu menu = new Menu(Menu_Top_Callback);
	
	if(StrEqual(sFieldName, "wins"))
		menu.SetTitle("%T", "MenuTitle_Top_Wins", client);
	else if(StrEqual(sFieldName, "kills"))
		menu.SetTitle("%T", "MenuTitle_Top_Kills", client);
	else if(StrEqual(sFieldName, "knifekills"))
		menu.SetTitle("%T", "MenuTitle_Top_KnifeKills", client);
	else if(StrEqual(sFieldName, "playtime"))
		menu.SetTitle("%T", "MenuTitle_Top_Playtime", client);
	else if(StrEqual(sFieldName, "points"))
		menu.SetTitle("%T", "MenuTitle_Top_Points", client);
	else if(StrEqual(sFieldName, "elo"))
		menu.SetTitle("%T", "MenuTitle_Top_ELO", client);
	else if(StrEqual(sFieldName, "kdr"))
		menu.SetTitle("%T", "MenuTitle_Top_KDR", client);
	else 
	{
		LogError("Unknown field '%s' for top menu", sFieldName);
		delete menu;
		return;
	}
	
	int iCount;
	char sBuffer[128];
	
	while (SQL_FetchRow(hndl))
	{
		iCount++;
		
		int playerID = SQL_FetchInt(hndl, 0);
		
		char sInfo[32];
		IntToString(playerID, sInfo, sizeof(sInfo));
		
		char sName[32];
		SQL_FetchString(hndl, 1, sName, sizeof(sName));
		
		if(StrEqual(sFieldName, "wins"))
		{
			int wins = SQL_FetchInt(hndl, 2);
			Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_Wins", client, iCount, sName, wins);
			menu.AddItem(sInfo, sBuffer);
		}
		else if(StrEqual(sFieldName, "kills"))
		{
			int kills = SQL_FetchInt(hndl, 2);
			Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_Kills", client, iCount, sName, kills);
			menu.AddItem(sInfo, sBuffer);
		}
		else if(StrEqual(sFieldName, "knifekills"))
		{
			int kills = SQL_FetchInt(hndl, 2);
			Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_KnifeKills", client, iCount, sName, kills);
			menu.AddItem(sInfo, sBuffer);
		}
		else if(StrEqual(sFieldName, "points"))
		{
			int points = SQL_FetchInt(hndl, 2);
			Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_Points", client, iCount, sName, points);
			menu.AddItem(sInfo, sBuffer);
		}
		else if(StrEqual(sFieldName, "elo"))
		{
			int elo = SQL_FetchInt(hndl, 2);
			Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_ELO", client, iCount, sName, elo);
			menu.AddItem(sInfo, sBuffer);
		}
		else if(StrEqual(sFieldName, "kdr"))
		{
			float kdr = SQL_FetchFloat(hndl, 2);
			Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_KDR", client, iCount, sName, kdr);
			menu.AddItem(sInfo, sBuffer);
		}
		else if(StrEqual(sFieldName, "playtime"))
		{
			int playtime = SQL_FetchInt(hndl, 2);
			int mins, hours, days, weeks;
			SplitPlaytime(playtime, weeks, days, hours, mins);
			
			if(weeks > 0)
			{
				Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_PlaytimeW", client, iCount, sName, mins, hours, days, weeks);
				menu.AddItem(sInfo, sBuffer);
			}
			else if(days > 0)
			{
				Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_PlaytimeD", client, iCount, sName, mins, hours, days);
				menu.AddItem(sInfo, sBuffer);
			}
			else if(hours > 0)
			{
				Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_PlaytimeH", client, iCount, sName, mins, hours);
				menu.AddItem(sInfo, sBuffer);
			}
			else
			{
				Format(sBuffer, sizeof(sBuffer), "%T", "MenuItem_Top_PlaytimeM", client, iCount, sName, mins);
				menu.AddItem(sInfo, sBuffer);
			}
		}
	}
		
	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

stock void SplitPlaytime(int playtime, int &weeks, int &days, int &hours, int &mins)
{
	mins = playtime;
	
	if (mins >= 10080)
	{
		weeks = RoundToFloor(mins / 10080.0);
		mins = mins % 1440;
    }
	
	if (mins >= 1440)
	{
		days = RoundToFloor(mins / 1440.0);
		mins = mins % 1440;
    }
	
	if (mins >= 60)
	{
		hours = RoundToFloor(mins / 60.0);
		mins = mins % 60;
    }
}

public int Menu_Top_Callback(Menu menu, MenuAction action, int client, int info)
{
	if (action == MenuAction_Select)
	{
		char sInfo[32];
		menu.GetItem(info, sInfo, sizeof(sInfo));
		
		
	}
	else if(action == MenuAction_End)
		delete menu;
}