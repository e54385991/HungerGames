Handle g_hVoiceTimer = null;

void ResetVoiceTimer()
{
	if(g_hVoiceTimer != null)
	{
		CloseHandle(g_hVoiceTimer);
		g_hVoiceTimer = null;
	}
}

void StartVoiceTimer()
{
	ResetVoiceTimer();
	
	g_hVoiceTimer = CreateTimer(1.0, Timer_Voice, _, TIMER_REPEAT);
}

public Action Timer_Voice(Handle timer, any data)
{
	if(!g_pZcoreVoice)
		return Plugin_Continue;
		
	LoopIngameClients(iSender)
	{
		LoopIngameClients(iReceiver)
		{
			bool bListen;
			
			if(RoundStatus <= RS_DelayedRoundEnd)
				bListen = true;
			else if(IsPlayerAlive(iSender) && IsPlayerAlive(iReceiver))
			{
				// Tributes hear tributes (including range check if needed)
				if(g_bIsTribute[iSender] && g_bIsTribute[iReceiver] && (g_fVoiceTributeRange <= 0.0 || Entity_InRange(iSender, iReceiver, g_fVoiceTributeRange)))
					bListen = true;
				// Non-tributes can hear non-tributes
				else if(!g_bIsTribute[iSender] && !g_bIsTribute[iReceiver])
					bListen = true;
			}
			// Dead players can hear dead players
			else if(!IsPlayerAlive(iSender) && !IsPlayerAlive(iReceiver))
				bListen = true;
			
			ZCore_Voice_SetListenOverride(iReceiver, iSender, bListen, g_iVoiceImmunity);
		}
	}
	
	return Plugin_Continue;
}

public Action ZCore_Voice_OnListenOverride(int iReceiver, int iSender, bool bListen, int iImmunity)
{
	if(iImmunity < g_iVoiceImmunity)
		return Plugin_Stop;
	
	return Plugin_Continue;
}