#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Hunger Games"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Hunger Games"
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <geoip>
#include <soundlib>

#include <hungergames>

#include <smlib> // Collection of stocks

#include <csgocolors> // Chat colors for CS:GO
#include <emitsoundany> // Allows to precache sounds in CS:GO
#include <map_workshop_functions> // Adds workshop map support

#undef REQUIRE_PLUGIN

#include <CustomPlayerSkins> // Used for the glow effect
bool g_pCustomPlayerSkins = false;

#include <zcore/zcore_lootspawner> // Spawns the loot
bool g_pZcoreLootSpawner = false;

#include <zcore/zcore_voice> // Handles listen overrides
bool g_pZcoreVoice = false;

#include <zcore/zcore_mysql> // Handles listen overrides
bool g_pZcoreMysql = false;

/* Offsets */

int g_iVelocity = -1;
int g_iFreeze = -1;

/* UserMsgs */

UserMsg g_FadeUserMsgId;

/* General */

bool g_bIsWarmup;
float g_fRoundStart = 0.0;
RoundStates RoundStatus = RS_FreezeTime;
bool g_bNoWinner;
int g_iLastWinner;
int g_iAnnual;

bool g_bIsTribute[MAXPLAYERS+1];
int g_iTributes;

Handle g_hTimer = null;

int g_iWepType[MAXPLAYERS + 1];
bool g_bCanZoom[MAXPLAYERS + 1];
bool g_bHasSilencer[MAXPLAYERS + 1];
bool g_bShowDeathMsg[MAXPLAYERS + 1];
int g_iKiller[MAXPLAYERS + 1];

char g_sCMD[][] = {"coverme", "takepoint", "holdpos", "regroup", "followme", "takingfire", "go", "fallback", "sticktog", "getinpos", "stormfront", "report", "roger", "enemyspot", "needbackup", "sectorclear", "inposition", "reportingin", "getout", "negative","enemydown"};

int g_iAnnounceStep;

/* Forwards */

Handle g_OnEndgameStart;
Handle g_OnRoundEnd;
Handle g_OnTributeSpawnPre;
Handle g_OnTributeSpawn;
Handle g_OnTributeFallen;
Handle g_OnDelayedRoundEnd;
Handle g_OnDistrictChanged;

/* Sub Files */

#include "hungergames/hg_cfg.sp"
#include "hungergames/hg_other.sp"
#include "hungergames/hg_stats.sp"
#include "hungergames/hg_game.sp"
#include "hungergames/hg_damage.sp"
#include "hungergames/hg_downloader.sp"
#include "hungergames/hg_party.sp"
#include "hungergames/hg_give_knife.sp"
#include "hungergames/hg_zombies.sp"
#include "hungergames/hg_loottables.sp"
#include "hungergames/hg_loot.sp"
#include "hungergames/hg_knockout.sp"
#include "hungergames/hg_tools.sp"
#include "hungergames/hg_fireworks.sp"
#include "hungergames/hg_endgame.sp"
#include "hungergames/hg_events.sp"
#include "hungergames/hg_survival.sp"
#include "hungergames/hg_hud.sp"
#include "hungergames/hg_glow.sp"
#include "hungergames/hg_gravity.sp"
#include "hungergames/hg_mapcleaner.sp"
#include "hungergames/hg_voice.sp"
#include "hungergames/hg_tagrenade.sp"
#include "hungergames/hg_shop.sp"
#include "hungergames/hg_spec.sp"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

/* Handle Plugins */

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("hunger-games");
	
	CreateNative("HG_GetRoundState", Native_GetRoundState);
	CreateNative("HG_GetLastRoundWinner", Native_GetLastRoundWinner);
	
	CreateNative("HG_IsTribute", Native_IsTribute);
	CreateNative("HG_SetTribute", Native_SetTribute);
	
	CreateNative("HG_HasDistrict", Native_HasDistrict);
	CreateNative("HG_GetDistrict", Native_GetDistrict);
	CreateNative("HG_SetDistrict", Native_SetDistrict);
	CreateNative("HG_GetDistrictMemberID", Native_GetDistrictMemberID);
	CreateNative("HG_GetDistrictLimit", Native_GetDistrictLimit);
	
	CreateNative("HG_GetHunger", Native_GetHunger);
	CreateNative("HG_SetHunger", Native_SetHunger);
	CreateNative("HG_AddHunger", Native_AddHunger);
	CreateNative("HG_ResetHunger", Native_ResetHunger);
	
	CreateNative("HG_GetEnergy", Native_GetEnergy);
	CreateNative("HG_SetEnergy", Native_SetEnergy);
	CreateNative("HG_AddEnergy", Native_AddEnergy);
	CreateNative("HG_ResetEnergy", Native_ResetEnergy);
	
	CreateNative("HG_GetThirst", Native_GetThirst);
	CreateNative("HG_SetThirst", Native_SetThirst);
	CreateNative("HG_AddThirst", Native_AddThirst);
	CreateNative("HG_ResetThirst", Native_ResetThirst);
	
	CreateNative("HG_GetStamina", Native_GetStamina);
	CreateNative("HG_SetStamina", Native_SetStamina);
	CreateNative("HG_AddStamina", Native_AddStamina);
	CreateNative("HG_ResetStamina", Native_ResetStamina);
	
	g_OnEndgameStart = CreateGlobalForward("HG_OnEndgameStart", ET_Ignore, Param_Float);
	g_OnRoundEnd = CreateGlobalForward("HG_OnRoundEnd", ET_Ignore, Param_Cell);
	g_OnTributeSpawnPre = CreateGlobalForward("HG_OnTributeSpawnPre", ET_Event, Param_Cell);
	g_OnTributeSpawn = CreateGlobalForward("HG_OnTributeSpawn", ET_Ignore, Param_Cell);
	g_OnTributeFallen = CreateGlobalForward("HG_OnTributeFallen", ET_Ignore, Param_Cell);
	g_OnDelayedRoundEnd = CreateGlobalForward("HG_OnDelayedRoundEnd", ET_Ignore, Param_Float);
	g_OnDistrictChanged = CreateGlobalForward("HG_OnDistrictChanged", ET_Ignore, Param_Cell, Param_Cell, Param_Cell, Param_Cell);
	
	__pl_zcore_lootspawner_SetNTVOptional();
	__pl_zcore_voice_SetNTVOptional();
	__pl_zcore_mysql__SetNTVOptional();
	__pl_CustomPlayerSkins_SetNTVOptional();
	
	return APLRes_Success;
}

public void OnLibraryAdded(const char[] name)
{
	if (StrEqual(name, "zcore-lootspawner"))
		g_pZcoreLootSpawner = true;
	else if (StrEqual(name, "zcore-voice"))
		g_pZcoreVoice = true;
	else if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = true;
	else if (StrEqual(name, "CustomPlayerSkins"))
		g_pCustomPlayerSkins = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "zcore-lootspawner"))
		g_pZcoreLootSpawner = false;
	else if (StrEqual(name, "zcore-voice"))
		g_pZcoreVoice = false;
	else if (StrEqual(name, "zcore-mysql"))
		g_pZcoreMysql = false;
	else if (StrEqual(name, "CustomPlayerSkins"))
		g_pCustomPlayerSkins = false;
}

/* Plugin Start */

public void OnPluginStart()
{
	/* Plugins */
	
	g_pZcoreLootSpawner = LibraryExists("zcore-lootspawner");
	g_pZcoreVoice = LibraryExists("zcore-voice");
	g_pZcoreMysql = LibraryExists("zcore-mysql");
	g_pCustomPlayerSkins = LibraryExists("CustomPlayerSkins");
	
	/* Convars */
	
	CreateConVars();
	
	/* Offsets */
	
	g_iVelocity = FindSendPropInfo("CBasePlayer", "m_vecVelocity[0]");
	g_iFreeze = FindSendPropInfo("CBasePlayer", "m_fFlags");
	
	/* UserMsgs */

	g_FadeUserMsgId = GetUserMessageId("Fade");
	
	/* Hooks */
	
	HookEvent("player_team", Event_PlayerTeam, EventHookMode_Pre);
	HookEvent("player_disconnect", Event_PlayerDisconnect, EventHookMode_Pre);
	HookEvent("player_connect", Event_PlayerConnect, EventHookMode_Pre);
	
	HookEvent("round_start", Event_RoundStart);
	HookEvent("round_freeze_end", Event_RoundFreezeEnd);
	HookEvent("round_end", Event_RoundEnd);
	
	HookEvent("player_spawn", Event_Spawn);
	
	HookEvent("player_death", Event_Death);
	HookEvent("player_death", Event_Death_Pre, EventHookMode_Pre);
	
	HookEvent("player_jump", Event_Jump);
	
	HookEvent("weapon_fire", Event_WeaponFire);
	HookEvent("item_equip", Event_ItemEquip);
	
	HookEvent("tagrenade_detonate", OnTagrenadeDetonate);
	
	AddCommandListener(Cmd_spec_next, "spec_next");
	AddCommandListener(Cmd_spec_prev, "spec_prev");
	AddCommandListener(Cmd_spec_player, "spec_player");
	AddCommandListener(Cmd_spec_mode, "spec_mode");
	
	AddCommandListener(Command_FlashlightToogle, "+lookatweapon");
	
	HookUserMessage(GetUserMessageId("TextMsg"), MsgHook_AdjustMoney, true);
	
	/* Admin Commands */
	
	RegAdminCmd("hg_knife_arena_set", Cmd_Set_KnifeArena, ADMFLAG_ROOT);
	RegAdminCmd("hg_knife_arena_remove", Cmd_Remove_KnifeArena, ADMFLAG_ROOT);
	RegAdminCmd("hg_event", Cmd_Event, ADMFLAG_ROOT);
	RegAdminCmd("hg_give_points", Cmd_GivePoints, ADMFLAG_ROOT);
	
	/* Commands */
	
	AddCommandListener(Command_Say, "say");
	AddCommandListener(Command_Say, "say_team");
	
	for(int i; i < sizeof(g_sCMD); i++)
		AddCommandListener(Command_BlockRadio, g_sCMD[i]);
	
	/* Translations */
	
	LoadTranslations("common.phrases");
	LoadTranslations("hungergames.phrases");
	LoadTranslations("hungergames-loot.phrases");
	LoadTranslations("hungergames-shop.phrases");
	LoadTranslations("hungergames-stats.phrases");
	LoadTranslations("hungergames-weapons.phrases");
	
	/* Timers */
	
	StartPlayerCheckTimer();
	StartHUDTimer();
	StartSurvivalTimer();
	StartCleanupTimer();
	StartVoiceTimer();
	StartEventTimer();
	StartZombieSpeedTimer();
	StartGlowTimer();
	StartPlaytimeTimer();
	
	/* Other */
	
	g_fRoundStart = GetGameTime();
	
	/* Mysql */
	ConnectDB();
	
	LoopIngameClients(client)
	{
		OnClientPutInServer(client);
		AuthPlayer(client);
	}
}

public void OnMapStart()
{
	CleanMapCrates();
	
	PrepareSounds();
	PrepareMaterialsAndModels();
	DownloadFiles();
	
	RegisterLoot();
	
	ResetPartys();
	ResetFireworks();
	
	RoundStatus = RS_FreezeTime;
	g_fRoundStart = GetGameTime();
	
	int iCSPlayerManager = GetPlayerResourceEntity();
	if (iCSPlayerManager != -1) 
		SDKHook(iCSPlayerManager, SDKHook_ThinkPost, Hook_OnThinkPost_Players);
	
	CreateTimer(40.0, Timer_Announce, _, TIMER_FLAG_NO_MAPCHANGE|TIMER_REPEAT);
}

public void OnMapEnd()
{
	ResetPartys();
	ResetZombieTimers();
	
	ResetAuthTimers();
	ResetFallenTributesTimer(false);
	
	LoopClients(client)
	{
		ResetKnockout(client, true);
		ResetTools(client);
	}
	
	RoundStatus = RS_BetweenMaps;
}

public void OnClientPutInServer(int client)
{
	ResetDelayedDeath(client);
	
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
	SDKHook(client, SDKHook_GroundEntChangedPost, OnGroundEntChanged);
}

public void OnClientDisconnect(int client)
{
	LeaveParty(client);
	
	SDKUnhook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
	SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);
	SDKUnhook(client, SDKHook_GroundEntChangedPost, OnGroundEntChanged);
	
	ResetPlayer(client);
}

public void OnClientPostAdminCheck(int client)
{
	AuthPlayer(client);
	if(g_bSndWelcome)
		EmitSoundToClientAny(client, sndWelcome);
}

public Action CS_OnTerminateRound(float &delay, CSRoundEndReason &reason)
{
	// Detect if game trys to restart itself...this roundend reason is calling no round_start event :/
	if(reason == CSRoundEnd_TerroristsSurrender || reason == CSRoundEnd_CTSurrender)
	{
		g_bNoWinner = true;
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

/* CSGO Events */

public Action Event_RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	CleanMapCrates();
	
	if(g_bPartyResetRoundstart)
		ResetPartys();
	
	ClearThrowingKnifes();
	
	RoundStatus = RS_FreezeTime;
	g_fRoundStart = GetGameTime();
	
	if ((GetRandomInt(0, 100) < 90 || !g_bSndRoundstart2) && g_bSndRoundstart)
		EmitSoundToAllAny(sndRoundStart);
	else EmitSoundToAllAny(sndRoundStart2);
	
	StartHUDTimer(); // We like to be sync for the countdown
	
	char sSuffix[MAXPLAYERS+1][4];
	LoopIngamePlayers(i)
	{
		GetSuffix(g_iAnnual, sSuffix[i], i);
		CPrintToChat(i, "%T", "Roundstart_Annual", i, g_sPrefix, g_iAnnual, sSuffix[i]);
	}
	
	return Plugin_Continue;
}

public Action Timer_Announce(Handle timer, any data)
{
	g_iAnnounceStep++;
	
	LoopIngamePlayers(i)
	{
		if(g_iAnnounceStep == 1 && g_iPartyLimit < 2)
			g_iAnnounceStep++;
		if(g_iAnnounceStep == 2 && !g_bKnockoutFake)
			g_iAnnounceStep++;
			
		
		if(g_iAnnounceStep == 1)
			CPrintToChat(i, "%T", "AnnouncePartySystem", i, g_sPrefix, g_iPartyLimit);
		else if(g_iAnnounceStep == 2)
			CPrintToChat(i, "%T", "AnnounceFakeDeath", i, g_sPrefix, g_sFakedeathCmds);
		else if(g_iAnnounceStep == 3)
			CPrintToChat(i, "%T", "AnnounceStatsMe", i, g_sPrefix, g_sStatsCmds);
		else if(g_iAnnounceStep == 4)
			CPrintToChat(i, "%T", "AnnounceTop", i, g_sPrefix, g_sTopCmds);
		else if(g_iAnnounceStep == 5)
		{
			CPrintToChat(i, "%T", "AnnounceSponsorShop", i, g_sPrefix, g_sShopCmds);
			g_iAnnounceStep = 0;
		}
	}
	
	return Plugin_Continue;
}

public Action Event_RoundFreezeEnd(Handle event, const char[] name, bool dontBroadcast)
{
	ServerCommand("mp_ignore_round_win_conditions 1");
	
	RoundStatus = RS_InProgress;
	g_fRoundStart = GetGameTime();
	ResetZombieTimers();
	StartLootWipetimer();
	
	SetNextEvent(true);
	
	LoopAlivePlayers(i)
	{
		if(g_bIsTribute[i])
			g_iTributes++;
	}
	
	return Plugin_Continue;
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	ResetZombieTimers();
	
	RoundStatus = RS_BetweenRounds;
	
	return Plugin_Continue;
}

public Action Event_Spawn(Handle event, const char[] name, bool dontBroadcast)
{
	int userID = GetEventInt(event, "userid");
	int client = GetClientOfUserId(userID);
	
	CreateTimer(1.5, Timer_SetClanTag, userID, TIMER_FLAG_NO_MAPCHANGE);
	
	g_bIsTribute[client] = false;
	
	ResetTagging(client);
	ResetFakeDeath(client);
	ResetDelayedDeath(client);
	ResetArmor(client);
	ResetTools(client);
	ResetThrowingKnifes(client);
	ResetBlockPrimaryTools(client);
	SetThirdPersonView(client, false);
	SetClientGravity(client, 1.0);
	ResetArmsModel(client);
	Entity_SetCollisionGroup(client, COLLISION_GROUP_DEBRIS_TRIGGER);
	
	if(IsZombie(client))
	{
		ApplyZombie(client);
		CreateTimer(0.3, Timer_ApplyZombie, GetClientUserId(client));
		return Plugin_Continue;
	}
	
	Action result;
	Call_StartForward(g_OnTributeSpawnPre);
	Call_PushCell(client);
	Call_Finish(result);
	
	/* If another plugin breaks this the player is not a tribute, useful to spawn similar roles like zombies */
	if(result == Plugin_Handled || result == Plugin_Stop)
		return Plugin_Continue;
	
	g_bIsTribute[client] = true;
		
	Call_StartForward(g_OnTributeSpawn);
	Call_PushCell(client);
	Call_Finish();
	
	RemoveHudDelayed(client);
	
	return Plugin_Continue;
}

public Action Timer_SetClanTag(Handle timer, any userID)
{
	int client = GetClientOfUserId(userID);
	
	if(!(0 < client && client <= MaxClients && IsClientInGame(client)) || IsFakeClient(client))
		return Plugin_Handled;
	
	char sTag[32];
	
	if (g_iWins[client] <= 0)
		Format(sTag, sizeof(sTag), "NEW");
	else Format(sTag, sizeof(sTag), "[%d Wins]", g_iWins[client]);
	
	CS_SetClientClanTag(client, sTag);
	
	return Plugin_Handled;
}

public Action Event_Jump(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(!g_bIsTribute[client])
		return Plugin_Continue;
	
	float stamina;
	HG_GetStamina(client, stamina);
	
	stamina -= g_fStaminaJumpCost;
	
	if(stamina < 0.0)
		stamina = 0.0;
	
	HG_SetStamina(client, stamina);
	
	return Plugin_Continue;
}

public Action Event_Death_Pre(Handle event, const char[] name, bool dontBroadcast)
{
	int victim = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(!victim)
		return Plugin_Continue;
	
	// Allow fake death events
	if(g_bShowDeathMsg[victim])
		return Plugin_Continue;
	
	ResetKnockout(victim, false);
	SetThirdPersonView(victim, false);
	
	if(IsZombie(victim))
		ResetArmsModel(victim);
	
	// Don't show death msgs
	return Plugin_Handled;
}

public Action Event_Death(Handle event, const char[] name, bool dontBroadcast)
{
	int victim = GetClientOfUserId(GetEventInt(event, "userid"));
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	
	g_iKiller[victim] = attacker;
	
	if(g_bShowDeathMsg[victim])
	{
		g_bShowDeathMsg[victim] = false;
		return Plugin_Continue;
	}
	
	char sWeaponName[64];
	GetEventString(event, "weapon", sWeaponName, sizeof(sWeaponName));
	
	char sWpItemID[64];
	GetEventString(event, "weapon_itemid", sWpItemID, sizeof(sWpItemID));
	
	char sWpFauxItemID[64];
	GetEventString(event, "weapon_fauxitemid", sWpFauxItemID, sizeof(sWpFauxItemID));
	
	char sWpOwnerUID[64];
	GetEventString(event, "weapon_originalowner_xuid", sWpOwnerUID, sizeof(sWpOwnerUID));
	
	bool hs = GetEventBool(event, "headshot");
	
	int dom = GetEventInt(event, "dominated");
	int rev = GetEventInt(event, "revenge");
	
	int pen = GetEventInt(event, "penetrated");
	
	Player_Death(attacker, victim, sWeaponName, sWpItemID, sWpFauxItemID, sWpOwnerUID, hs, dom, rev, pen);
	
	return Plugin_Continue;
}

public Action Event_WeaponFire(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	char sWeapon[32];
	GetEventString(event, "weapon", sWeapon, 32);
	
	if(g_bIsTribute[client] && StrContains(sWeapon, "knife") != -1)
		ThrowKnife(client);
	
	return Plugin_Continue;
}

public Action Event_ItemEquip(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_iWepType[client] = GetEventInt(event, "weptype");
	
	g_bCanZoom[client] = GetEventBool(event, "canzoom");
	g_bHasSilencer[client] = GetEventBool(event, "hassilencer");
}

public Action Command_Say(client, const char[] command, int argc)
{
	// Ignore console
	if(!client)
		return Plugin_Continue;

	char sText[192]
	char sBuffer[24];
	GetCmdArgString(sText, sizeof(sText));
	
	int iStart;
	if(sText[strlen(sText) - 1] == '"')
	{
		sText[strlen(sText) - 1] = '\0';
		iStart = 1;
	}

	BreakString(sText[iStart], sBuffer, sizeof(sBuffer));
	
	if(GetTrieValue(g_hTrie_ShopCmds, sBuffer, iStart)) // iStart is not needed anmore thats why I'm using it here :3
	{
		Cmd_Shop(client);
		return Plugin_Handled;
	}
	
	if(GetTrieValue(g_hTrie_FakedeathCmds, sBuffer, iStart))
	{
		Cmd_FakeDeath(client);
		return Plugin_Handled;
	}
	
	if(GetTrieValue(g_hTrie_SuicideCmds, sBuffer, iStart))
	{
		Cmd_Suicide(client);
		return Plugin_Handled;
	}
	
	if(GetTrieValue(g_hTrie_StatsCmds, sBuffer, iStart))
	{
		Cmd_Stats(client);
		return Plugin_Handled;
	}
	
	if(GetTrieValue(g_hTrie_TopCmds, sBuffer, iStart))
	{
		Cmd_Top(client);
		return Plugin_Handled;
	}

	return Plugin_Continue;
}

/* HG Events*/

public void HG_OnEndgameStart(float roundTime)
{
	// Reset stuff
	
	ResetZombieTimers(false);
	
	// Slay non tribute players like zombies
	
	if(!g_bZombieEndgame)
	{
		LoopAlivePlayers(i)
		{
			if(g_bIsTribute[i])
				continue;
				
			ForcePlayerSuicide(i);
		}
	}
	
	// Announce
	
	LoopIngamePlayers(i)
	{
		CPrintToChat(i, "%T", "Endgame_Started", i, g_sPrefix);
	}
	
	if(g_bSndEndgame)
		EmitSoundToAllAny(sndEndgame);
	
	// Start Endgame
	
	StartEndgame();
}

public void HG_OnRoundEnd(int winner)
{
	/* Reregister loot groups (in case they changed) */
	RegisterLoot();
	
	Stats_Roundend(winner);
	
	g_iAnnual++;
	
	// Cache suffix for each player
	
	char sSuffix[MAXPLAYERS+1][4];
	LoopIngamePlayers(i)
	{
		GetSuffix(g_iAnnual, sSuffix[i], i);
	}
	
	// No winner
	if(!Entity_IsPlayer(winner))
	{
		LoopIngamePlayers(i)
		{
			CPrintToChat(i, "%T", "Roundend_NoWinner", i, g_sPrefix, g_iAnnual, sSuffix[i]);
		}
		
		if(g_bSndRoundDraw)
			EmitSoundToAllAny(sndRoundDraw);
			
		return;
	}
	
	char sName[64];
	if(!Client_IsValid(winner))
		Format(sName, sizeof(sName), "WORLD");
	else Format(sName, sizeof(sName), "%N", winner);
	
	int district = GetParty(winner);
	
	if(district == -1)
	{
		LoopIngamePlayers(i)
		{
			CPrintToChat(i, "%T", "Roundend_Winner", i, g_sPrefix, sName, g_iAnnual, sSuffix[i]);
		}
	}
	else 
	{
		LoopIngamePlayers(i)
		{
			CPrintToChat(i, "%T", "Roundend_WinnerDistrict", i, g_sPrefix, sName, district, g_iAnnual, sSuffix[i]);
		}
	}
	
	CS_SetMVPCount(winner, CS_GetMVPCount(winner)+1);
	StartFireworks(winner);
}

public void HG_OnTributeSpawn(int client)
{
	// Survival Stuff
	
	HG_ResetHunger(client);
	HG_ResetEnergy(client);
	HG_ResetThirst(client);
	HG_ResetStamina(client);
	
	// Reset Weapons
	
	ResetThrowingKnifes(client);
	StripPlayerWeapons(client, true, false);
	
	// Give delayed items
	
	GiveKnifeDelayed(client);
	GiveBinocularsDelayed(client);
	GiveCompassDelayed(client);
	GiveParachuteDelayed(client);
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(g_bIsTribute[client])
	{
		CheckParty(client, buttons);
		CheckLoot(client, buttons, weapon);
		
		if(GetGameTime() > g_fBlockPrimaryTools[client])
		{
			if(!HasBinocularsEnabled(client))
				CheckParachute(client, buttons, weapon);
		
			if(!HasParachuteEnabled(client))
				CheckBinoculars(client, buttons, weapon);
		}
		else
		{
			DisableParachute(client);
			DisableBinoculars(client);
		}
	}
	
	AdminSetKnifeArena(client, buttons);
  	
  	return Plugin_Continue;
}

/* Block radio commands */

public Action Command_BlockRadio(int client, const char[] command, int args) 
{
	return Plugin_Handled;
}

/* Block team change messages */

public Action Event_PlayerTeam(Handle event, const char[] name, bool dontBroadcast)
{
	return Plugin_Handled;
}

/* Block player connect messages */

public Action Event_PlayerConnect(Handle event, const char[] name, bool dontBroadcast)
{
	return Plugin_Handled;
}

/* Block player disconnect messages */

public Action Event_PlayerDisconnect(Handle event, const char[] name, bool dontBroadcast)
{
	return Plugin_Handled;
}